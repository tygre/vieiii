/* ================================================================================ */
/*                                                                                  */
/* Vie III, la nouvelle g�n�ration                                                  */
/*		Programm� par Tygre                                                         */
/*                                                                                  */
/* Cette version marche qu'avec le 2.x ou 3.x (je n'ai pas encore tout test�)		*/
/* Version 0.19                                                                     */
/*                                                                                  */
/* ================================================================================ */



/* --------------------------------- */
/* Les 'includes' pour mon programme */
/* --------------------------------- */

#include <exec/types.h>
#include <exec/memory.h>

#include <devices/timer.h>

#include <intuition/intuition.h>
#include <intuition/classes.h>
#include <intuition/classusr.h>
#include <intuition/imageclass.h>
#include <intuition/gadgetclass.h>
#include <intuition/pointerclass.h>

#include <libraries/gadtools.h>
#include <libraries/asl.h>

#include <graphics/displayinfo.h>
#include <graphics/gfxbase.h>

#include <clib/asl_protos.h>
#include <clib/exec_protos.h>
#include <clib/intuition_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/dos_protos.h>

#include <proto/timer.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "fortify.h"




/* ================================================================================ */
/*                          On initialise les fonctions					            */
/* ================================================================================ */



#include "Configuration.h" /* Tout pour la fen�tre de configurationn               */
#include "Controle.h"      /* Le panneau de contr�le du jeu de la vie              */
#include "Plateau.h"       /* La d�finition de la fen�tre `Plateau`                */
#include "Informations.h"  /* La fen�tre d'informations                            */
#include "Interface.h"     /* Les chargements/sauvegardes                          */
#include "JDLV.h"          /* Le jeu de la vie classique                           */
#include "Publicite.h"     /* Tout pour la fen�tre de pub                          */
#include "VieIII.h"

#include "Fortify.h"



void AfficheTexte( struct Window *pWindow,
		UBYTE CoulTexte,
		UBYTE CoulFond,
		UWORD XGauche,
		UWORD YHaut,
		char *pChaine );                   /* Affiche du texte dans la fen�tre d�sir�e        */
int  main( BYTE ArgC, char *ArgV[] );      /* La boucle principale */



/* ================================================================================ */
/*           On cr�er la structure `Configuration` et on l'initialise			*/
/* ================================================================================ */



struct Conf Config =
{
	50,
	50,
	50,
	50,
	FALSE,

	FALSE,
	0,
	"RAM:",

	TRUE,
	TRUE,

	FALSE,

	0,
	11,

	TRUE,
	0,
	0,

	386,
	172
};



/* ================================================================================ */
/*                           D�claration des varaibles					            */
/* ================================================================================ */



/* La police de charact�re utilis�e par le programme */
struct TextAttr Ma_police =
{
	(STRPTR)"topaz.font",    /* La plus moche           */
	8,                       /* 80/40 caract�res        */
	0x00,                    /* Normale                 */
	0x42                     /* Elle est sur disque (?) */
};

static struct Library *IntuitionBase;
static struct Library *AslBase;
static struct Library *GadToolsBase;
static struct Library *GFXBase;

struct Screen  *Mon_ecran;   /* L'�cran que c'est moi qui l'ouvre */
APTR            VisualInfo;

BOOL ConfigModifiee = FALSE;   /* Pour savoir si la configuration a �t� modifi�e puis sauv�e en ENV: */

static char ErrorMessages[17][70] =
{
	"Impossible d'ouvrir la librairie Intuition et/ou ASL",
	"Impossible d'ouvrir l'�cran",
	"Impossible d'ouvrir la fen�tre du plateau",
	"Impossible d'ouvrir la publicit�",
	"5",
	"Erreur pendant la cr�ation du fichier `ENVARC:VieIII.config`",
	"7",
	"Impossible de cr�er le tableau `AdrTab`",
	"Impossible de cr�er les tableaux `AdrX` et `AdrY`",
	"Impossible de cr�er le tableau `AdrTab1`",
	"Impossible de cr�er les tableaux `AdrX1` et `AdrY1`",
	"12",
	"Impossible d'ouvrir la fen�tre de configuration",
	"14",
	"Erreur pendant la lecture du fichier `ENVARC:VieIII.config`",
	"Impossible d'ouvrir la fen�tre de contr�le",
	"Impossible d'ouvrir la fen�tre d'infromations"
};

USHORT ZzzPointer[36]=
{
	0x0000, 0x0000,  /* Utilis� par Intuition seulement */
	0x0300, 0x0000,
	0x1F9C, 0x0300,
	0x3FFE, 0x1F9C,
	0x63E3, 0x3FFE,
	0x7A3B, 0x3FFE,
	0xF7B7, 0x7FFE,
	0xEF63, 0x7FFE,
	0xE23F, 0x7FFE,
	0x7FFE, 0x3FC0,
	0x3fC0, 0x0F80,
	0x0FB0, 0x0000,
	0x0078, 0x0030,
	0x0030, 0x0000,
	0x0004, 0x0000,
	0x000E, 0x0004,
	0x0004, 0x0000,
	0x0000, 0x0000  /* Utilis� par Intuition seulement */
};



/* ================================================================================ */
/*                              L'�cran du logiciel						*/
/* ================================================================================ */



/* ----------------------------------------------- */
/* D�claration, d�finition et ouverture de l'�cran */
/* ----------------------------------------------- */

int OuvreEcran( void )
{
	WORD DriPens[] = { ~0 };
	struct Screen *WBEcran = (struct Screen *)OpenWorkBench();

	if( Config.Ecran == FALSE )
	{
		if( ! ( Mon_ecran = OpenScreenTags(
									NULL,
									SA_Left,          0,
									SA_Top,           0,
									SA_Width,         WBEcran->Width,
									SA_Height,        WBEcran->Height,
									SA_Depth,         3,
									SA_Font,          &Ma_police,
									SA_Type,          CUSTOMSCREEN,
									SA_DisplayID,     PAL_MONITOR_ID | HIRES_KEY,
									SA_Pens,          DriPens,
									SA_Title,         "Vie III",
									TAG_DONE ) ) )
			return( 2L );
	}
	else
	{
		if ( ! ( Mon_ecran = LockPubScreen( (UBYTE * )"Workbench" ) ) )
			return( 2L );
	}



	if( ! ( VisualInfo = GetVisualInfo( Mon_ecran, TAG_DONE ) ) )
		return( 1L );

	return( 0L );
}



/* --------------------------------------- */
/* Fonction pour fermer proprement l'�cran */
/* --------------------------------------- */

void FermeEcran( void )
{
	if( VisualInfo )
	{
		FreeVisualInfo( VisualInfo );
		VisualInfo = NULL;
	}

	if( Config.Ecran == FALSE )
		CloseScreen( Mon_ecran );
	else
		UnlockPubScreen( NULL, Mon_ecran );

	Mon_ecran = NULL;
}



/* ================================================================================ */
/*         D�finition de ma fonction pour avoir un requetser simplement			*/
/* ================================================================================ */



/* ---------------------------------------------------------- */
/* Definition de l'affichage du message, du "Oui" et du "Non" */
/* ---------------------------------------------------------- */

static struct IntuiText TexteAlerte[]=
{
	{
		1,            /* FrontPen                                     */
		0,            /* BackPen                                      */
		JAM2,         /* DrawMode                                     */
		0, 0,         /* LeftEdge, TopEdge (seront modifi� plus tard) */
		&Ma_police,   /* ITextFont, ma font                           */
		NULL,         /* IText (sera modifi� plus tard)               */
		NULL          /* NextText, pas d'autres structures connect�es */
	},
	{
		1, 0, JAM2, 0, 0, &Ma_police, NULL, NULL
	},
	{
		1, 0, JAM2, 0, 0, &Ma_police, NULL, NULL
	}
};



/* ------------------------------------------------------------ */
/*                                                              */
/* Ma fonction pour afficher un joli requester simplement       */
/*                                                              */
/* Alerte( struct Window *pWindow,   un pointeur sur un fen�tre */
/*         char *pMessage,           un pointeur sur du texte   */
/*         char *pOui,               idem                       */
/*         char *pNon )              idem                       */
/*                                                              */
/* ------------------------------------------------------------ */

BOOL Alerte( pWindow, pMessage, pOui, pNon )

struct Window *pWindow;
char *pMessage,*pOui,*pNon;

{
	/* Affectation des messages dans les structures IntuiText */
	TexteAlerte[0].IText = pMessage;
	TexteAlerte[1].IText = pOui;
	TexteAlerte[2].IText = pNon;

	/* Oh ! le joli requester */
	return( AutoRequest( pWindow, &TexteAlerte[0],
	                              &TexteAlerte[1],
								  &TexteAlerte[2], 0, 0, 0, 0 ) );
}



/* ================================================================================ */
/*                D�finition de la fonction d'affichage de texte				*/
/* ================================================================================ */



/* ---------------------------------- */
/* Definition de l'affichage du texte */
/* ---------------------------------- */

static struct IntuiText TexteAffiche =
{
	1,            /* FrontPen                                     */
	0,            /* BackPen                                      */
	JAM2,         /* DrawMode                                     */
	0, 0,         /* LeftEdge, TopEdge (seront modifi� plus tard) */
	&Ma_police,   /* ITextFont, ma font                           */
	NULL,         /* IText (sera modifi� plus tard)               */
	NULL          /* NextText, pas d'autres structures connect�es */
};



/* ------------------------------------------------------------------------------ */
/*                                                                                */
/* Ma fonction pour afficher du texte dans une fen�tre :                          */
/*                                                                                */
/* AfficheTexte( pWindow,        Un pointeur sur une structure Window             */
/*               CoulTexte,      La couleur du texte                              */
/*               CoulFond,       La couleur du fond                               */
/*               XGauche,        La coordonn�e en X du texte                      */
/*               YHaut,          La coordonn�e en Y du texte                      */
/*               pChaine )       Un pointeur sur la chaine � afficher             */
/*                                                                                */
/* ------------------------------------------------------------------------------ */

void AfficheTexte( pWindow, CoulTexte, CoulFond, XGauche, YHaut, pChaine  )

struct Window *pWindow;
UBYTE CoulTexte, CoulFond;
UWORD XGauche, YHaut;
char *pChaine;

{
	/* On copie la chaine � afficher dans `my_text` */
	TexteAffiche.IText = pChaine;

	/* On change les couleur dans la structure */
	TexteAffiche.FrontPen = CoulTexte;
	TexteAffiche.BackPen  = CoulFond;

	/* On change les coordonnees d'affichage du texte */
	TexteAffiche.LeftEdge = XGauche;
	TexteAffiche.TopEdge  = YHaut;

	/* On affiche la chaine DANS la fen�tre ! */
	PrintIText( pWindow->RPort, &TexteAffiche, 0, 0 );
}



/* ================================================================================ */
/*                   Pour tout fermer d'un coup et s'en aller				*/
/* ================================================================================ */



void CleanExit( ErrorCode )

int ErrorCode;

{
	if( Config.WorkBench == FALSE )   // Si le WorkBench est ferm�
		OpenWorkBench();            // Et bien on l'ouvre !



	if( ErrorCode > 0 )
	{
		Alerte( NULL, ErrorMessages[ErrorCode - 1],
				  "Quoi !?",
				  "Damned !" );
	}



	if( FenetrePub )
		FermePub();

	if( FenetreConf )
		FermeConf();

	if( FenetreInfos )
		FermeInfos();

	if( FenetrePlateau )
		FermePlateau();

	if( FenetreControle )
		FermeControle();

	if( Mon_ecran )
		FermeEcran();

	if( IntuitionBase )
		CloseLibrary( IntuitionBase );

	if( AslBase )
		CloseLibrary( AslBase );

	if( GadToolsBase )
		CloseLibrary( GadToolsBase );

	if( GFXBase )
		CloseLibrary( GFXBase );

	if( AdrTab || AdrX || AdrY )
		FreeRemember( &Memoire, TRUE );

	if( AdrTab1 || AdrX1 || AdrY1 )
		FreeRemember( &Memoire1, TRUE );

	Fortify_LeaveScope();
	Fortify_OutputStatistics();

	exit( ErrorCode );
}



/* ================================================================================ */
/*                             La boucle principale						*/
/* ================================================================================ */



int	main( ArgC, ArgV )

BYTE ArgC;
char *ArgV[];

{
	ULONG Classe;                      // Les IDCMPFlags
	APTR Adresse;                      // L'Adresse de c'qui nous a envoy� un message
	UWORD Code;                        // !!!!
	UWORD BP;                          // Contiendra le num�ro du bouton press�
	struct IntuiMessage *Le_message;   // Pour les messages d'Intuition



	// Ease spotting and debugging memory problems
	Fortify_EnterScope();

	
	
	/* --------------------------------------------- */
	/* On doit ouvrir les librairie Intuition et Asl */
	/* --------------------------------------------- */

	IntuitionBase = OpenLibrary( "intuition.library", 39 );
		  AslBase = OpenLibrary( "asl.library",       38 );
	 GadToolsBase = OpenLibrary( "gadtools.library",  39 );
		  GFXBase = OpenLibrary( "graphics.library",  39 );

	if( !( IntuitionBase && AslBase && GadToolsBase && GFXBase ) )
		CleanExit(1);



	/* ------------------ */
	/* On ouvre la config */
	/* ------------------ */

	LitConf();


	/* ------------------------------------------------------ */
	/* Ouverture de l'ecran en fonction de la version de l'OS */
	/* ------------------------------------------------------ */

	if( OuvreEcran() != 0 )
		CleanExit(2);



	/* ---------------------------------- */
	/* Ouverture de la fen�tre de pub ... */
	/* ---------------------------------- */

	// Publicite();



	/* --------------------------------------------------------- */
	/* Ouverture de la fen�tre en fonction de la version de l'OS */
	/* --------------------------------------------------------- */

	if( OuvrePlateau() != 0 )
		CleanExit(3);

	SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);

	UtiliseConf();



	/* --------------------- */
	/* On cr�er les tableaux */
	/* --------------------- */

	AlloueTableaux( TRUE );
	AfficheCellules();

	ClearPointer( FenetrePlateau );



	/* ----------------------------------------------------------------------- */
	/* On attend jusqu'a ce que l'utilisateur ai press� le gadget de fermeture */
	/* ----------------------------------------------------------------------- */

	while( TRUE )
	{
		/* --------------------------------------------- */
		/* On attend jusqu'� ce qu'on recoive un message */
		/* --------------------------------------------- */

		WaitPort( FenetrePlateau->UserPort );



		/* ------------------------------------------------------------------- */
		/* Tant qu'on re�oit bien les messages (si on en avait re�u plusieurs) */
		/* ------------------------------------------------------------------- */

		while( Le_message = (struct IntuiMessage *)GT_GetIMsg( FenetrePlateau->UserPort ) )
		{
			/* ------------------------------------------------------------ */
			/* Apr�s avoir collect� avec succ�s un message, on peut le lire */
			/* et l'enregistrer pour traitement ulterieur                   */
			/* ------------------------------------------------------------ */

			 Classe = Le_message->Class;
			   Code = Le_message->Code;
			Adresse = Le_message->IAddress;

			/* ------------------------------------------------------------- */
			/* Apr�s avoir lut le message on s'en va aussi vite que possible */
			/* ATTENTION ! Ne jamais essayer de lire un message apr�s �a     */
			/* ------------------------------------------------------------- */

			GT_ReplyIMsg( Le_message );



			/* ------------------------------------------ */
			/* On regarde quel message IDCMP a �t� envoy� */
			/* ------------------------------------------ */

			switch( Classe )
			{
			case IDCMP_CLOSEWINDOW:
				SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);
				if( Alerte( FenetrePlateau,  "Voulez-vous r�ellement quitter Vie III ?", "Oui", "Non" ) )
				{
					if( ConfigModifiee )
					{
						if( Alerte( FenetrePlateau, "La configuration a �t� modifi�e, quitter ?",
										    "Quitter",
										    "NON !!" ) )
							CleanExit( 0 );
					}
					else
					{
						CleanExit( 0 );
					}
				}
				ClearPointer( FenetrePlateau );
				break;

			case IDCMP_REFRESHWINDOW:
				GT_BeginRefresh( FenetrePlateau );
				DessinsPlateau();
				GT_EndRefresh( FenetrePlateau, TRUE );
				break;

			case IDCMP_MOUSEBUTTONS:
				if( Code == SELECTUP )
					PauseCellule();
				Informations( TRUE );
				break;

			case IDCMP_GADGETUP:
				for( BP = 0; Adresse != PlateauGadgets[BP] && BP < Plateau_CNT; BP++ );

				/* ------------------------- */
				/* Quel est le bouton press� */
				/* On met le pointeur Busy   */
				/* ------------------------- */

				switch( BP )
				{
				case GD_Nouveau:
					EffaceTableaux();
					Fantomize( TRUE );
					AfficheCellules();
					break;

				case GD_ScrollerH:
					ValeurH = Code;
					AfficheCellules();
					break;

				case GD_ScrollerV:
					ValeurV = Code;
					AfficheCellules();
					break;

				case GD_Charger:

					/* ------------------------------------------------- */
					/* ATTENTION ! La fonction charge inclue la fonction */
					/* AlloueTableaux( TRUE ) si un fichier est choisit  */
					/* ------------------------------------------------- */

					SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);
					Charge();
					ClearPointer( FenetrePlateau );
					break;

				case GD_Sauver:
					SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);
					Sauve( FALSE );
					ClearPointer( FenetrePlateau );
					break;

				case GD_SauverC:
					SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);
					Sauve( TRUE );
					ClearPointer( FenetrePlateau );
					break;

				case GD_Vivre:
					for( BP = GD_Nouveau; BP <= GD_Quitter; BP++ )
						OffGadget( PlateauGadgets[BP], FenetrePlateau, NULL );
					Controle();
					break;

				case GD_Conf:
					FermePlateau();             // On ferme le plateau
					Configuration();            // On ouvre la configuration
					if( OuvrePlateau() != 0 )   // On essaye de r�-ouvrir le plateau
						CleanExit(3);           // On se casse si �a loupe
					SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);
					UtiliseConf();              // On initialise le plateau avec la configuration
					AlloueTableaux( FALSE );    // On r�-alloue les tableaux sans perdre les Cellules
					AfficheCellules();          // On affiche les Cellules
					break;

				case GD_Infos:
					Informations( FALSE );
					break;

				case GD_Quitter:
					if( ConfigModifiee )
					{
						SetPointer( FenetrePlateau, ZzzPointer, 16, 16, 0, 0);
						if( Alerte( FenetrePlateau, "La configuration a �t� modifi�e, quitter ?",
										    "Quitter",
										    "NON !!" ) )
							CleanExit( 0 );
						ClearPointer( FenetrePlateau );
					}
					else
					{
						CleanExit( 0 );
					}
					break;

				}   /* Fin du `switch( BP )` */
				ClearPointer( FenetrePlateau );
				Informations( TRUE );
				break;


			}   /* Fin du `switch( Classe )` */

		}   /* Fin du `while(Le_message = ...` */


	}   /* Fin du `while( TRUE )` */

	return 0;
}



/* ================================================================================ */
/*                         Appel de la boucle principale					*/
/* ================================================================================ */




// int main();
