#ifndef PLATEAU_H
#define PLATEAU_H 1

#define GD_ScrollerH   0
#define GD_ScrollerV   1
#define GD_Nouveau     2
#define GD_Charger     3
#define GD_Sauver      4
#define GD_SauverC     5
#define GD_Imprimer    6
#define GD_Vivre       7
#define GD_Conf        8
#define GD_Infos       9
#define GD_Quitter    10

#define Plateau_CNT   11

extern struct Window *FenetrePlateau;
extern struct Gadget *PlateauGadgets[];
extern UWORD           OffX;
extern UWORD           OffY;

int  OuvrePlateau( void );       /* Pour ouvrir et initialiser le fen�tre `Plateau` */
void DessinsPlateau( void );     /* Les dessins de la fen�tre `Plateau`             */
void FermePlateau( void );       /* Pour fermer la fen�tre `Plateau`                */
void Fantomize( BOOL Shadow );   /* Pour les gadgets                                */

#endif


