/* ================================================================================ */
/*                                                                                  */
/* Vie III, la nouvelle g�n�ration                                                  */
/*		Programm� par Tygre                                                         */
/*                                                                                  */
/* ================================================================================ */
/*                                                                                  */
/* Les informations, revision 0.8.1                                                 */
/*                                                                                  */
/* ================================================================================ */



#include <clib/dos_protos.h>
#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/intuition_protos.h>
#include <exec/types.h>
#include <intuition/screens.h>
#include <utility/tagitem.h>
#include <stdio.h>

#include "VieIII.h"
#include "Informations.h"

#include "Fortify.h"



extern struct Conf Config;
extern int NBCels;
extern int NBGen;
extern int NBGen1;
extern UWORD OffX;
extern UWORD OffY;
extern struct Screen  *Mon_ecran;



/* ================================================================================ */
/*                           D�claration des varaibles					*/
/* ================================================================================ */



struct Window *FenetreInfos;                /* L'adresse de la fen�tre */
static LONG Time1[3], Time2[3], Time3[3];   /* Pour le temps */



/* ================================================================================ */
/*                    D�finition de la fen�tre `Informations`				*/
/* ================================================================================ */



/* -------------------------------------------------- */
/* D�claration, d�finition et ouverture de la fen�tre */
/* -------------------------------------------------- */

int OuvreInfos( void )
{
	UWORD OffY = Mon_ecran->WBorTop + Mon_ecran->RastPort.TxHeight + 1;

	if( ! ( FenetreInfos = OpenWindowTags(
							NULL,
							WA_Left,         Config.FenetreInfosLeft,
							WA_Top,          Config.FenetreInfosTop,
							WA_Width,        280,
							WA_Height,       62 + OffY,
							WA_IDCMP,        IDCMP_REFRESHWINDOW,
							WA_Flags,        WFLG_DRAGBAR|
											 WFLG_DEPTHGADGET|
											 WFLG_SMART_REFRESH,
							WA_Title,        "Informations",
							WA_CustomScreen, Mon_ecran,
							TAG_DONE ) ) )
		return( 1L );

	GT_RefreshWindow( FenetreInfos, NULL );

	return( 0L );
}



/* ---------------------- */
/* Pour fermer la fen�tre */
/* ---------------------- */

void FermeInfos( void )
{
	if( FenetreInfos )
	{
		CloseWindow( FenetreInfos );
		FenetreInfos = NULL;
	}
}



/* ================================================================================ */
/*                         Gestion des informations						*/
/* ================================================================================ */



void Informations( Modifier )

BOOL Modifier;

{
	char TexteAffichage[50];
	ULONG Ticks;

	if( Modifier == FALSE )
	{
		if( FenetreInfos )
		{
			FermeInfos();
			Config.FenetreInfos = FALSE;
		}
		else
		{
			DateStamp( (struct DateStamp *)Time1 );
			DateStamp( (struct DateStamp *)Time2 );
			if( OuvreInfos() != 0 )
				CleanExit(17);
			Config.FenetreInfos = TRUE;
		}
	}

	if( FenetreInfos )
	{
		/* --------------------------------- */
		/* On se rappelle de ses coordonnees */
		/* --------------------------------- */

		Config.FenetreInfosLeft = FenetreInfos->LeftEdge;
		Config.FenetreInfosTop  = FenetreInfos->TopEdge;

		/* --------------------------- */
		/* On affiche les informations */
		/* --------------------------- */

		sprintf( TexteAffichage, "%d Cellule(s)    ", NBCels );
		AfficheTexte( FenetreInfos, 1, 0, 6, 12, TexteAffichage );

		sprintf( TexteAffichage, "%d gen�rations    ", NBGen );
		AfficheTexte( FenetreInfos, 1, 0, 6, 22, TexteAffichage );

		sprintf( TexteAffichage, "Reste %d octets    ", (int)( AvailMem( 1L<<1 ) + AvailMem( 1L<<2 ) ) );
		AfficheTexte( FenetreInfos, 1, 0, 6, 32, TexteAffichage );



		DateStamp( (struct DateStamp *)Time3 );

		sprintf( TexteAffichage, "%ld ticks/gen�ration    ",
			   Time3[2] - Time2[2] + (Time3[1] - Time2[1])*3000 + (Time3[0] - Time2[0])*4320000
			 );
		AfficheTexte( FenetreInfos, 1, 0, 6, 42, TexteAffichage );

		Ticks = Time3[2] - Time1[2] + (Time3[1] - Time1[1])*3000 + (Time3[0] - Time1[0])*4320000;
		if( Ticks != 0 )
		{
			sprintf( TexteAffichage, "%2.3f gen�ration(s)/secondes    ",
				   (float)NBGen1/Ticks*50 );
			AfficheTexte( FenetreInfos, 1, 0, 6, 52, TexteAffichage );
		}
	}
}
