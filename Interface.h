#ifndef INTERFACE_H
#define INTERFACE_H 1

APTR Selecteur( BOOL Sauvegarde,
		    char *Titre,
		    char *OK_Texte,
		    char *Repertoire );    /* La fonction pour le s�leteur de fichier */
int LireLigne( FILE *fp,
		   char *s );              /* Pour lire une ligne dans un fichier     */
void Charge( void );                   /* Pour charger une population             */
void Sauve( UBYTE SauverComme );       /* Pour sauver une population              */
void EcrireFichier( void );            /* Pour �crire les coords. dans le fichier */

#endif

