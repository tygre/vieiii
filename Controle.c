/* ================================================================================ */
/*                                                                                  */
/* Vie III, la nouvelle g�n�ration                                                  */
/*		Programm� par Tygre                                                         */
/*                                                                                  */
/* ================================================================================ */
/*                                                                                  */
/* Le panneau de cont�le, revision 0.5.1                                            */
/*                                                                                  */
/* ================================================================================ */



#include <clib/dos_protos.h>
#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/intuition_protos.h>
#include <exec/types.h>
#include <graphics/text.h>
#include <intuition/gadgetclass.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>
#include <libraries/gadtools.h>
#include <utility/tagitem.h>

#include "Controle.h"
#include "Informations.h"
#include "JDLV.h"
#include "Plateau.h"
#include "VieIII.h"

#include "Fortify.h"



#define GD_ScrollerH   0
#define GD_ScrollerV   1
#define GD_Controle_Vitesse       0
#define GD_Controle_Vivre         1
#define GD_Controle_Destination   2
#define GD_Controle_Aller         3
#define GD_Controle_Pas           4

#define Controle_CNT              5



// De VieIII.c
extern struct Conf Config;
extern struct TextAttr Ma_police;
extern int NBCels;
extern int NBGen1;
extern UWORD ValeurH;
extern UWORD ValeurV;
extern struct Screen  *Mon_ecran;
extern APTR VisualInfo;



/* ================================================================================ */
/*                           D�claration des varaibles					*/
/* ================================================================================ */



struct Window *FenetreControle;

static struct Gadget *ControleGList = NULL;
static struct Gadget *ControleGadgets[Controle_CNT];
static LONG Time1[3], Time2[3], Time3[3];



/* ================================================================================ */
/*                   D�finition de la fen�tre `Configuration`				*/
/* ================================================================================ */



/* -------------------- */
/* Les diff�rents types */
/* -------------------- */

static UBYTE *VivreLabels[] =
{
	(UBYTE *)"Stop",
	(UBYTE *)"Vivre",
	(UBYTE *)"Aller �",
	NULL
};



/* -------- */
/* Le texte */
/* -------- */

static struct IntuiText  ControleIText[] =
{
	1, 0, JAM1, 82, 52, &Ma_police, (UBYTE *)"la",         &ControleIText[1],
	1, 0, JAM1,162, 52, &Ma_police, (UBYTE *)"gen�ration", NULL
};



/* ------------------- */
/* Le type des Gadgets */
/* ------------------- */

static UWORD ControleGTypes[] =
{
	SCROLLER_KIND,
	CYCLE_KIND,
	INTEGER_KIND,
	BUTTON_KIND,
	INTEGER_KIND
};



/* ---------------------------------------------------------- */
/* D�finition des gadgets de le fen�tre `Panneau de Contr�le` */
/* ---------------------------------------------------------- */

static struct NewGadget ControleNGad[] =
{
	 70,  5, 172, 10, (UBYTE *)"Vitesse", NULL, GD_Controle_Vitesse,     PLACETEXT_LEFT, NULL, NULL,
	151, 25,  91, 14, NULL,               NULL, GD_Controle_Vivre,       0,              NULL, NULL,
	104, 49,  51, 14, NULL,               NULL, GD_Controle_Destination, 0,              NULL, NULL,
	  5, 49,  71, 14, (UBYTE *)"Aller �", NULL, GD_Controle_Aller,       PLACETEXT_IN,   NULL, NULL,
	 70, 25,  51, 14, (UBYTE *)"Pas",     NULL, GD_Controle_Pas,         PLACETEXT_LEFT, NULL, NULL
};



/* ----------------------------------- */
/* Tags des Gadgets de `Configuration` */
/* ----------------------------------- */

static ULONG ControleGTags[] =
{
	(GTSC_Total), 51, (GTSC_Visible), 1, (GTSC_Arrows), 16, (PGA_Freedom), LORIENT_HORIZ, (GA_RelVerify), TRUE, (TAG_DONE),
	(GTCY_Labels), (ULONG)&VivreLabels[0], (TAG_DONE),
	(GTIN_Number), 0, (GTIN_MaxChars), 10, (TAG_DONE),
	(TAG_DONE),
	(GTIN_Number), 1, (GTIN_MaxChars),  3, (TAG_DONE)
};



/* -------------------------------------------------- */
/* D�claration, d�finition et ouverture de la fen�tre */
/* -------------------------------------------------- */

int OuvreControle( void )
{
	struct NewGadget ng;
	struct Gadget *g;
	UWORD lc, tc;
	UWORD OffX = Mon_ecran->WBorLeft,
		OffY = Mon_ecran->WBorTop + Mon_ecran->RastPort.TxHeight + 1;

	if( !( g = CreateContext( &ControleGList ) ) )
		return( 1L );

	for( lc = 0, tc = 0; lc < Controle_CNT; lc++ )
	{

		CopyMem( (char *)&ControleNGad[lc], (char *)&ng, (long)sizeof( struct NewGadget ) );

		ng.ng_VisualInfo = VisualInfo;
		ng.ng_TextAttr   = &Ma_police;
		ng.ng_LeftEdge  += OffX;
		ng.ng_TopEdge   += OffY;

		ControleGadgets[lc] = g = CreateGadgetA( (ULONG)ControleGTypes[lc],
								     g,
								     &ng,
								     (struct TagItem *)&ControleGTags[tc] );

		while( ControleGTags[tc] )
			tc += 2;
		tc++;

		if ( NOT g )
			return( 2L );
	}

	if( !( FenetreControle = OpenWindowTags( NULL,
							 WA_Left,          Config.FenetreControleLeft,
							 WA_Top,           Config.FenetreControleTop,
							 WA_Width,         254,
							 WA_Height,        73 + OffY,
							 WA_IDCMP,         SCROLLERIDCMP|
										 ARROWIDCMP|
										 CYCLEIDCMP|
										 INTEGERIDCMP|
										 BUTTONIDCMP|
										 IDCMP_CLOSEWINDOW|
										 IDCMP_REFRESHWINDOW,
							 WA_Flags,         WFLG_DRAGBAR|
										 WFLG_DEPTHGADGET|
										 WFLG_CLOSEGADGET|
										 WFLG_SMART_REFRESH|
										 WFLG_ACTIVATE,
							 WA_Gadgets,       ControleGList,
							 WA_Title,         (UBYTE *)"Panneau de contr�le",
							 WA_CustomScreen,  Mon_ecran,
							 TAG_DONE ) ) )
		return( 4L );

	GT_RefreshWindow( FenetreControle, NULL );

	DessinsControle();

	return( 0L );
}



/* ------------------------ */
/* Pour afficher les textes */
/* ------------------------ */

void DessinsControle( void )
{
	UWORD OffX, OffY;

	OffX = FenetreControle->BorderLeft;
	OffY = FenetreControle->BorderTop;

	PrintIText( FenetreControle->RPort, ControleIText, OffX, OffY );
}



/* ---------------------- */
/* Pour fermer la fen�tre */
/* ---------------------- */

void FermeControle( void )
{
	if( FenetreControle )
	{
		CloseWindow( FenetreControle );
		FenetreControle = NULL;
	}

	if( ControleGList )
	{
		FreeGadgets( ControleGList );
		ControleGList = NULL;
	}
}



/* ================================================================================ */
/*                        Gestion du panneau de contr�le					*/
/* ================================================================================ */



void Controle( void )
{
	BOOL Quitter = FALSE, QuitterVivre;
	ULONG Classe;            /* Les IDCMPFlags                              */
	APTR Adresse;            /* L'adresse de c'qui nous a envoy� un message */
	UWORD Code, Vitesse = 0;
	UBYTE BP = 0;
	struct IntuiMessage *Le_message;
	struct IntuiMessage *Le_message2;



	if( OuvreControle() != 0 )
		CleanExit(16);



	/* ----------------------------------------------------------------------- */
	/* On attend jusqu'a ce que l'utilisateur ai press� le gadget de fermeture */
	/* ----------------------------------------------------------------------- */

	while( Quitter == FALSE )
	{
		/* --------------------------------------------- */
		/* On attend jusqu'� ce qu'on recoive un message */
		/* --------------------------------------------- */

		WaitPort( FenetreControle->UserPort );

		/* ------------------------------------------------------------------- */
		/* Tant qu'on re�oit bien les messages (si on en avait re�u plusieurs) */
		/* ------------------------------------------------------------------- */

		while( Le_message = (struct IntuiMessage *)GT_GetIMsg( FenetreControle->UserPort ) )
		{
			/* ------------------------------------------------------------ */
			/* Apr�s avoir collect� avec succ�s un message, on peut le lire */
			/* et l'enregistrer pour traitement ulterieur                   */
			/* ------------------------------------------------------------ */

			 Classe = Le_message->Class;
			   Code = Le_message->Code;
			Adresse = Le_message->IAddress;



			/* -------------------------------------------------------------------- */
			/* Apr�s avoir lut le message on s'en va aussi vite que possible        */
			/* ATTENTION ! Ne jamais essayer de lire un message apr�s s'�tre repli� */
			/* -------------------------------------------------------------------- */

			GT_ReplyIMsg( Le_message );

			/* ------------------------------------------ */
			/* On regarde quel message IDCMP a �t� envoy� */
			/* ------------------------------------------ */

			switch( Classe )
			{
			case IDCMP_CLOSEWINDOW:
				/* Gadget de fermeture selectionn� */
				Quitter = TRUE;
				break;

			case IDCMP_REFRESHWINDOW:
				GT_BeginRefresh( FenetreControle );
				DessinsControle();
				GT_EndRefresh( FenetreControle, TRUE );
				break;

			case IDCMP_GADGETUP:
				/* Quel bouton relach� ? */
				for( BP=0; Adresse != ControleGadgets[BP]; BP++ );

				switch( BP )
				{
				case GD_Controle_Vitesse:
					Vitesse = Code;
					break;
				case GD_Controle_Vivre:
					FantomizeControle( TRUE );
					switch( Code )
					{
					case 1:
						DateStamp( (struct DateStamp *)Time1 );
						NBGen1 = 0;
						QuitterVivre = FALSE;
						while( QuitterVivre == FALSE )
						{
							DateStamp( (struct DateStamp *)Time2 );
							JDLV();
							Informations( TRUE );
							Delay( Vitesse );

							if( NBCels == 0)
							{
								QuitterVivre = TRUE;
								Quitter = TRUE;
							}

							/* -------------------------------------- */
							/* Testes pour les scrollers de `Plateau` */
							/* -------------------------------------- */

							Le_message2 = (struct IntuiMessage *)GT_GetIMsg( FenetrePlateau->UserPort );
							if( Le_message2 != NULL && Le_message2->Class == IDCMP_GADGETUP )
							{
								if( Le_message2->IAddress == PlateauGadgets[GD_ScrollerH] )
								{
 									ValeurH = Le_message2->Code;
									AfficheCellules();
								}

								if( Le_message2->IAddress == PlateauGadgets[GD_ScrollerV] )
								{
 									ValeurV = Le_message2->Code;
									AfficheCellules();
								}
							}

							/* ----------------------------------- */
							/* Les boutons de la fen�tre `Cont�le` */
							/* ----------------------------------- */

							Le_message = (struct IntuiMessage *)GT_GetIMsg( FenetreControle->UserPort );
							if( Le_message != NULL)
							{
								 Classe = Le_message->Class;
								   Code = Le_message->Code;
								Adresse = Le_message->IAddress;

								switch( Classe )
								{
								case IDCMP_CLOSEWINDOW:
									QuitterVivre = TRUE;
									Quitter = TRUE;
									break;
								case IDCMP_GADGETUP:
									if( Adresse == ControleGadgets[GD_Controle_Vivre] && Code == 0 )
										QuitterVivre = TRUE;
									if( Adresse == ControleGadgets[GD_Controle_Vitesse] )
										Vitesse = Code;
									break;
								default:
									break;
								}
							}
						}
						/* Fin du `while( QuitterVivre == FALSE )` */
						break;
					}
					/* Fin du `switch( Code )` */

					FantomizeControle( FALSE );

					break;
				}
				break;

			}   /* Fin du `switch( Classe )` */

		}   /* Fin du `while( Le_message = ...` */

	}   /* Fin du `while( Quitter == FALSE )` */


	Fantomize( FALSE );
	if( NBCels == 0 )
		Fantomize( TRUE );


	/* On se rappelle de ses coordonnees */
	Config.FenetreControleLeft = FenetreControle->LeftEdge;
	Config.FenetreControleTop  = FenetreControle->TopEdge;

	/* On ferme le panneau de controle */
	FermeControle();
}



/* ================================================================================ */
/*                 Pour mettre en ghost ou en normal les boutons				*/
/* ================================================================================ */



void FantomizeControle( Shadow )

BOOL Shadow;

{
	BYTE NB;

	if( Shadow )
	{
		for( NB = 2; NB <= 4; NB++ )
			OffGadget( ControleGadgets[NB], FenetreControle, NULL );
	}
	else
	{
		for( NB = 2; NB <= 4; NB++ )
			OnGadget( ControleGadgets[NB], FenetreControle, NULL );
	}
}
