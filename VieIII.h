#ifndef VIEIII_H
#define VIEIII_H 1

#define DEBUG FALSE

struct Conf
{
	UWORD ResolutionH;
	UWORD ResolutionV;	/* La r�solution du plateau       */
	UWORD VisibleH;
	UWORD VisibleV;		/* La partie visible du plateau   */
	UBYTE Plateau;		/* Type de plateau :              */
					/*        FALSE => Circulaire     */
					/*        TRUE  => Ferm�          */

	BOOL  SauveAuto;       /* Sauvegardes automatiques :     */
				      /*        FALSE ou TRUE           */
	UWORD Temps; 	      /* Temps entre chaque sauvegardes */
	char  Repertoire[256];   /* Le tiroir o� elles se feront   */

	UBYTE WorkBench;   /* L'�cran du WorkBench           */
				 /*        FALSE => Ferm�          */
				 /*        TRUE  => Ouvert         */
	UBYTE Ecran;		/* Type d'�cran :                 */
					/*        FALSE => Custom         */
					/*        TRUE  => WorkBench      */

	UBYTE Regles;	/* Les r�gles :                   */
				/*        FALSE => Jeu  de la Vie */
				/*        TRUE  => Utilisateur    */

	UWORD FenetrePlateauLeft;   /* XGauche de la fen�tre du plateau */
	UWORD FenetrePlateauTop;    /* YHaut de la fen�tre du plateau   */

	BOOL  FenetreInfos;        /* Doit-elle �tre imm�diatement ouverte ? */
	UWORD FenetreInfosLeft;   /* XGauche de la fen�tre d'infos          */
	UWORD FenetreInfosTop;    /* YHaut de la fen�tre d'infos            */

	UWORD FenetreControleLeft;   /* XGauche de la fen�tre de contr�le */
	UWORD FenetreControleTop;    /* YHaut de la fen�tre de contr�le   */
};

extern struct Conf Config;



int  OuvreEcran( void );

void FermeEcran( void );

BOOL Alerte(
	struct Window *,
	char *,
	char *,
	char * );

void CleanExit( int );

void AfficheTexte(
	struct Window *,
	UBYTE,
	UBYTE,
	UWORD,
	UWORD,
	char *);

#endif
