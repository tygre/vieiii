/* ================================================================================ */
/*														                            */
/* Vie III, la nouvelle g�n�ration									                */
/*		Programm� par Tygre              							                */
/*														                            */
/* ================================================================================ */
/*														                            */
/* Le jeu de la vie, revision 0.15.1                                                */
/*														                            */
/* ================================================================================ */



#include <clib/graphics_protos.h>
#include <clib/intuition_protos.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <stdio.h>

#include "JDLV.h"
#include "Plateau.h"
#include "VieIII.h"

#include "Fortify.h"



#define COULEUR_CELLULE 2



BOOL  **AdrTab;
BOOL  **AdrTab1;
struct Remember *Memoire = NULL;    /* Structure Remember pour AdrTab  */
struct Remember *Memoire1 = NULL;   /* Structure Remember pour AdrTab1 */

UWORD  *AdrX;
UWORD  *AdrY;
UWORD  *AdrX1;
UWORD  *AdrY1;

UWORD   LargeurCase,   /* La largeur d'une Cellules                   */
		HauteurCase,   /* et sa hauteur (le plateau fait 400 * 200)   */
	    ValeurH = 0,
		ValeurV = 0;   /* Les valeurs de chaque Scroller              */
int     NBCels = 0,    /* Le nombre de Cellules                       */
	    NBGen = 0,     /* Le nombre de gen�ration                     */
	    NBGen1 = 0;    /* Le nombre de gen�ration a pour chaque appel */



/* ================================================================================ */
/*         Fonction d'affichage des Cellules en fonction des Scrollers			*/
/* ================================================================================ */



void AfficheCellules( void )
{
	register int NB;
	UWORD LargeurGFX = LargeurCase * Config.VisibleH,
		  HauteurGFX = HauteurCase * Config.VisibleV;

	/* ------------------------------------------- */
	/* On �fface tout ce qu'il y a dans le plateau */
	/* ------------------------------------------- */

	SetAPen( FenetrePlateau->RPort, 0 );
	RectFill( FenetrePlateau->RPort, OffX + 43,
						   OffY +  9,
						   OffX + 43 + LargeurGFX - 1,
						   OffY +  9 + HauteurGFX - 1 );

	/* ---------------------------------------------------*/
	/* Un petit probl�me se pr�sente ...                  */
	/*   Exemple :                                        */
	/* Le plateau fait 400 * 200. Si on rentre une        */
	/* visibilit� de, disons, 30 * 30 on obtient          */
	/* LargeurCase = 400 / 30 = ... 13 ! En effet         */
	/* LargeurCase est un UWORD donc entier, ce qui cr�er */
	/* un leger probl�me (aboutissant au Mega-Bug).       */
	/* -------------------------------------------------- */

	if( LargeurGFX < 400 )
	{
		SetAPen( FenetrePlateau->RPort, 6 );
		RectFill( FenetrePlateau->RPort, OffX + 43 + LargeurGFX,
							   OffY +  9,
							   OffX + 43 + 400,
							   OffY +  9 + 200 );
	}
	if( HauteurGFX < 200 )
	{
		SetAPen( FenetrePlateau->RPort, 6 );
		RectFill( FenetrePlateau->RPort, OffX + 43,
							   OffY +  9 + HauteurGFX,
							   OffX + 43 + 400,
							   OffY +  9 + 200 );
	}

	/* ------------------------------------------------------------------ */
	/* On affiche toutes les Cellules visibles                            */
	/* On met un < car il y a bien Config.VisibleH Cellules visibles mais */
	/* elles sont num�rot�es � partir de 0                                */
	/* ------------------------------------------------------------------ */

	for( NB = 0; NB < NBCels; NB++ )
	{
		if( CELLULE( CCX(NB),CCY(NB) ) )
			TraceCellule( CCX(NB),CCY(NB), COULEUR_CELLULE );
	}
}



/* ================================================================================ */
/*        Fonctions d'allocation de la m�moire : allocation dynamique !!		*/
/* ================================================================================ */



void AlloueTableaux( Nouveau )

BOOL Nouveau;

{
	register int NB, NBCels1;



	/* ----------------------- */
	/* La taille d'une Cellule */
	/* ----------------------- */

	LargeurCase = 400 / Config.VisibleH,
	HauteurCase = 200 / Config.VisibleV;



	/* ---------------------------------------------------- */
	/* On recopie le Cellules dans le tableau AdrX1 & AdrY1 */
	/* ---------------------------------------------------- */

	if( Nouveau == FALSE )
	{
		NBCels1 = NBCels;

		for( NB = 0; NB < NBCels; NB++ )
		{
			if( DEBUG )
			{
				printf( "(JDLV) CCX( %d ) = %d\n", NB, (int) CCX( NB ) );
				printf( "(JDLV) CCY( %d ) = %d\n", NB, (int) CCY( NB ) );
			}

			CCX1( NB ) = CCX( NB );
			CCY1( NB ) = CCY( NB );

			if( DEBUG )
			{
				printf( "(JDLV) CCX1( %d ) = %d\n", NB, (int) CCX1( NB ) );
				printf( "(JDLV) CCY1( %d ) = %d\n", NB, (int) CCY1( NB ) );
			}
		}
	}

	/* -------------------------------- */
	/* Maintenant on �fface AdrTab & Co */
	/* -------------------------------- */

	FreeRemember( &Memoire, TRUE );

	 NBCels = 0;

	/* ------------------------------------------------- */
	/* D'abord on r�serve le nombre de lignes du tableau */
	/* ------------------------------------------------- */

	AdrTab = (BOOL **)AllocRemember( &Memoire, Config.ResolutionV * sizeof( BOOL * ), MEMF_PUBLIC|MEMF_CLEAR );

	if( AdrTab == NULL )
		CleanExit( 8 );

	/* --------------------------------------------- */
	/* Puis pour chaque lignes, le nombre de colones */
	/* --------------------------------------------- */

	for( NB = 0; NB < Config.ResolutionV; NB++ )
	{
		*(AdrTab + NB) = (BOOL *)AllocRemember( &Memoire, Config.ResolutionH * sizeof( BOOL ), MEMF_PUBLIC|MEMF_CLEAR );
		if( !(*(AdrTab + NB)) )
			CleanExit( 8 );
	}

	/* --------------------------------------------- */
	/* Allocation de la m�moire pour les coordonn�es */
	/* --------------------------------------------- */

	AdrX = (UWORD *)AllocRemember( &Memoire,
						 Config.ResolutionH * Config.ResolutionV * sizeof( UWORD ),
						 MEMF_PUBLIC|MEMF_CLEAR );
	AdrY = (UWORD *)AllocRemember( &Memoire,
						 Config.ResolutionH * Config.ResolutionV * sizeof( UWORD ),
						 MEMF_PUBLIC|MEMF_CLEAR );

	if( AdrX == NULL || AdrY == NULL )
		CleanExit( 9 );



	/* ----------------------------------------------------------------------- */
	/* On recopie l'ancien tableau dans le nouveau (dans les limites du stock) */
	/* ----------------------------------------------------------------------- */

	if( Nouveau == FALSE )
	{
		if( DEBUG )
			printf( "(JDLV) NBCels1=%d\n", NBCels1 );

		for( NB = 0; NB < NBCels1; NB++ )
		{
			if( DEBUG )
			{
				printf( "(JDLV) CCX1( %d ) = %d\n", NB, (int) *(AdrTab + NB) );
				printf( "(JDLV) CCY1( %d ) = %d\n", NB, (int) *(AdrTab + NB) );
			}

			if( CCX1( NB ) < Config.ResolutionH && CCY1( NB ) < Config.ResolutionV )
			{
				CCX( NBCels ) = CCX1( NB );
				CCY( NBCels ) = CCY1( NB );
				if( DEBUG )
				{
					printf( "(JDLV) CCX( %d ) = %d\n", NBCels, (int) CCX( NBCels ) );
					printf( "(JDLV) CCY( %d ) = %d\n", NBCels, (int) CCY( NBCels ) );
				}
				CELLULE( CCX( NBCels ), CCY( NBCels ) ) = TRUE;
				NBCels++;
			}
		}
	}

	/* ------------------------------------------------------------------- */
	/* On �fface AdrTab1 & Co et on les recr�er avec les nouvelles tailles */
	/* ------------------------------------------------------------------- */

	if( AdrTab1 || AdrX1 || AdrY1 )
	{
		FreeRemember( &Memoire1, TRUE );   /* On �vacue la structure Memoire */
	}

	AdrTab1 = (BOOL **)AllocRemember( &Memoire1, Config.ResolutionV * sizeof( BOOL * ), MEMF_PUBLIC|MEMF_CLEAR );

	if( AdrTab1 == NULL )
		CleanExit( 10 );

	for( NB = 0; NB < Config.ResolutionV; NB++ )
	{
		*(AdrTab1 + NB) = (BOOL *)AllocRemember( &Memoire1, Config.ResolutionH * sizeof( BOOL ), MEMF_PUBLIC|MEMF_CLEAR );
		if( !(*(AdrTab1 + NB)) )
			CleanExit( 10 );
	}

	AdrX1 = (UWORD *)AllocRemember( &Memoire1,
						  Config.ResolutionH * Config.ResolutionV * sizeof( UWORD ),
						  MEMF_PUBLIC|MEMF_CLEAR );
	AdrY1 = (UWORD *)AllocRemember( &Memoire1,
						  Config.ResolutionH * Config.ResolutionV * sizeof( UWORD ),
						  MEMF_PUBLIC|MEMF_CLEAR );

	if( AdrX1 == NULL || AdrY1 == NULL )
		CleanExit( 11 );

	if( DEBUG )
		printf( "(JDLV) NBCels=%d\n", NBCels );

	if( NBCels == 0 )
		Fantomize( TRUE );      // On shadowize les gadgets
}



/* ================================================================================ */
/*             Fonction g�rant la pause et l'enlevement des Cellules			*/
/* ================================================================================ */



void PauseCellule( void )
{
	/* ---------------------------------------- */
	/* Les coordonn�es de la souris par rapport */
	/* au coin haut-gauche du plateau           */
	/* ---------------------------------------- */

	UWORD XSouris = FenetrePlateau->MouseX - OffX - 43,
		YSouris = FenetrePlateau->MouseY - OffY - 9,
		XCase, YCase;
	int NB = 0;



	if( 0 < XSouris && 0 < YSouris && XSouris < (LargeurCase * Config.VisibleH) && YSouris < (HauteurCase * Config.VisibleV) )
	{
		/* ------------------------------------ */
		/* Recherche des coordonn�es de la case */
		/* ------------------------------------ */

		for( XCase = 0; LargeurCase * XCase < XSouris;
		     XCase++ );
		XCase--;
		for( YCase = 0; HauteurCase * YCase < YSouris;
		     YCase++ );
		YCase--;

		XCase += ValeurH;	
		YCase += ValeurV;   /* XCase et YCase sont donc les valeurs REELLES */

		if( CELLULE( XCase, YCase ) == FALSE )
		{
			/* --------------------------------------- */
			/* On affiche la Cellule et on la m�morise */
			/* --------------------------------------- */

			TraceCellule( XCase, YCase, COULEUR_CELLULE );
			CELLULE( XCase, YCase) = TRUE;
			CCX( NBCels ) = XCase;
			CCY( NBCels ) = YCase;
			NBCels++;

			if( NBCels == 1 )
				Fantomize( FALSE );
		}
		else
		{
			/* ------------------------------------------ */
			/* On recherche quelle est la Cellule enlev�e */
			/* ------------------------------------------ */

			for( NB = 0; CCX( NB ) != XCase || CCY( NB ) != YCase; NB++ )
				;

			/* ----------------------------------- */
			/* On d�gage la Cellule et on l'oublie */
			/* ------------------------------------ */

			TraceCellule( XCase, YCase, 0 );

			CCX( NB ) = CCX( NBCels - 1 );
			CCY( NB ) = CCY( NBCels - 1 );
			CELLULE( XCase, YCase ) = FALSE;
			NBCels--;

			if( NBCels == 0 )
				Fantomize( TRUE );
		}
	}
}



/* ================================================================================ */
/*                   La fonction du Jeu de la vie traditionnel				*/
/* ================================================================================ */



void JDLV( void )

{
	BOOL **AdrTab2;
	UWORD *AdrX2, *AdrY2;
	register WORD X, Y, NBCels1 = 0;
	register int NB;
	register BYTE I, J, NBPotes = 0;
	register WORD XD, YH, XG, YB;     // Et non BYTE !



	/* ----------------------------------- */
	/* C'est parti pour le Jeu de la vie ! */
	/* ----------------------------------- */

	NBGen++;
	NBGen1++;



	/* -------------------------- */
	/* On regarde chaques Cellule */
	/* -------------------------- */

	for( NB = 0; NB < NBCels; NB++ )
	{
		/* ------------------------------------------ */
		/* On regarde les Cellules autour de celle-ci */
		/* ------------------------------------------ */

		for( I = -1; I <= 1; I++ )
		{
			for( J = -1; J <= 1; J++ )
			{
				/* ----------------------------------- */
				/* Les coordonn�es de la case regard�e */
				/* ----------------------------------- */

				X = I + CCX( NB );
				Y = J + CCY( NB );

				/* ------------------------------------------- */
				/* Si le plateau est circulaire ..., sinon ... */
				/* ------------------------------------------- */

				if( !Config.Plateau )
				{
					if( X < 0 )
						X = Config.ResolutionH - 1;
					if( Y < 0 )
						Y = Config.ResolutionV - 1;
					if( X >= Config.ResolutionH )
						X = 0;
					if( Y >= Config.ResolutionV )
						Y = 0;
				}

				XD = X + 1;
				XG = X - 1;
				YH = Y - 1;
				YB = Y + 1;

				if( !Config.Plateau )
				{
					if( XG < 0 )
						XG = Config.ResolutionH - 1;
					if( YH < 0 )
						YH = Config.ResolutionV - 1;
					if( XD >= Config.ResolutionH )
						XD = 0;
					if( YB >= Config.ResolutionV )
						YB = 0;
				}

				NBPotes = CELLULE( XG, YH ) + CELLULE( X, YH )  +
					    CELLULE( XD, YH ) + CELLULE( XG, Y )  + 
					    CELLULE( XD, Y )  + CELLULE( XG, YB ) +
					    CELLULE( X, YB )  + CELLULE( XD, YB );

				if( !CELLULE1( X, Y )  && ( NBPotes == 3 || ( NBPotes == 2 && CELLULE( X, Y ) ) ) )
				{
					TraceCellule( X, Y, COULEUR_CELLULE );

					CELLULE1( X, Y ) = TRUE;
					CCX1( NBCels1 ) = X;
					CCY1( NBCels1 ) = Y;
					NBCels1++;
				}

				if( CELLULE( X, Y )  && ( NBPotes < 2 || NBPotes > 3 ) )
					TraceCellule( X, Y, 0 );

			}   /* Fin du `for( J = -1 ...` */
		}   /* Fin du `for( I = -1 ...` */
	}   /* Fin du `for( NB = 0 ...` */



	/* ----------------------------------------------------------- */
	/* On �change les tableaux AdrTab1 et AdrTab                   */
	/* Puis on �fface le contenue de AdrTab1 (anciennement AdrTab) */
	/* Enfin on �change les tableaux AdrX1, AdrY1 et AdrX, AdrY    */
	/* ----------------------------------------------------------- */

	AdrTab2 = AdrTab;
	 AdrTab = AdrTab1;
	AdrTab1 = AdrTab2;

	for( NB = 0; NB < NBCels; NB++ )
		CELLULE1( CCX( NB ), CCY( NB ) ) = FALSE;

	AdrX2 = AdrX;
	 AdrX = AdrX1;
	AdrX1 = AdrX2;

	AdrY2 = AdrY;
	 AdrY = AdrY1;
	AdrY1 = AdrY2;



	/* ------------------------------- */
	/* On change le nombre de Cellules */
	/* ------------------------------- */

	NBCels = NBCels1;
	NBCels1 = 0;



	/* ---------------------------------------- */
	/* On affiche toutes les Cellules d'un coup */
	/* ---------------------------------------- */

	/* AfficheCellules(); */
}



/* ================================================================================ */
/*                    Simple fonction d'affichage d'un carr�				*/
/* ================================================================================ */



void TraceCellule( XCase, YCase, Couleur )

UWORD XCase, YCase;
UBYTE Couleur;

{
	XCase -= ValeurH;
	YCase -= ValeurV;   /* XCase et YCase sont donc les valeurs RELATIVES */

	if( XCase < Config.VisibleH && YCase < Config.VisibleV )
	{
		SetAPen( FenetrePlateau->RPort, Couleur );
		RectFill( FenetrePlateau->RPort,OffX + 43 + XCase * LargeurCase,
						OffY +  9 + YCase * HauteurCase,
						OffX + 43 + (XCase + 1) * LargeurCase - 1,
						OffY +  9 + (YCase + 1) * HauteurCase - 1 );
	}
}



void EffaceTableaux( void )
{
	int i;
	
	for( i = 0; i < NBCels; i++ )
		CELLULE( CCX( i ), CCY( i ) ) = FALSE;
	NBGen = 0;
	NBCels = 0;
}

