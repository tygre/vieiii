/* ================================================================================ */
/*                                                                                  */
/* Vie III, la nouvelle g�n�ration                                                  */
/*		Programm� par Tygre                                                         */
/*                                                                                  */
/* ================================================================================ */
/*                                                                                  */
/* La configuration, revision 0.9.3                                                 */
/*                                                                                  */
/* ================================================================================ */



#include <clib/dos_protos.h>
#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/intuition_protos.h>
#include <exec/types.h>
#include <graphics/text.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>
#include <libraries/gadtools.h>
#include <utility/tagitem.h>
#include <stdlib.h>
#include <string.h>

#include "Configuration.h"
#include "Informations.h"
#include "Plateau.h"
#include "Publicite.h"
#include "VieIII.h"

#include "Fortify.h"



#define GD_Conf_ResolutionH    0
#define GD_Conf_ResolutionV    1
#define GD_Conf_VisibleH       2
#define GD_Conf_VisibleV       3
#define GD_Conf_TypePlateau    4
#define GD_Conf_SauveAuto      5
#define GD_Conf_Temps          6
#define GD_Conf_Repertoire     7
#define GD_Conf_WBEcran        8
#define GD_Conf_Ecran          9
#define GD_Conf_Regles        10
#define GD_Conf_Definition    11
#define GD_Conf_Sauver        12
#define GD_Conf_Utiliser      13
#define GD_Conf_Annuler       14
#define GD_Conf_Pub           15
#define GD_Conf_Idem          16

#define Conf_CNT              17

#define MIN(x,y)   ((x)<(y) ? (x):(y))

#define LARGEUR_MAX          200
#define HAUTEUR_MAX          200



// De VieIII.c
extern struct Conf Config;
extern struct TextAttr Ma_police;
extern UWORD ValeurH;
extern UWORD ValeurV;
extern struct Screen  *Mon_ecran;
extern APTR VisualInfo;
extern BOOL ConfigModifiee;
extern USHORT *ZzzPointer;
// De Informations.c
extern struct Window *FenetreInfos;
// De Plateau.c
extern struct Window *FenetrePlateau;
extern struct Gadget *PlateauGadgets[];



/* ================================================================================ */
/*                           D�claration des varaibles					*/
/* ================================================================================ */



struct Window *FenetreConf;   /* L'adresse de la fen�tre */

static struct Gadget *ConfGList;
static struct Gadget *ConfGadgets[Conf_CNT];
static ULONG ValeurModifiee[11];   /* Pour les modifications des gadgets */



/* ================================================================================ */
/*                   D�finition de la fen�tre `Configuration`				*/
/* ================================================================================ */



/* ------------------------------- */
/* Les diff�rents types de plateau */
/* ------------------------------- */

static UBYTE *TypePlateauLabels[] =
{
	(UBYTE *)"Circulaire",
	(UBYTE *)"Ferm�",
	NULL
};



/* ------------------------------ */
/* Quoi qu'on fait du Workbench ? */
/* ------------------------------ */

static UBYTE *WBEcranLabels[] =
{
	(UBYTE *)"Ferm�",
	(UBYTE *)"Ouvert",
	NULL
};



/* ----------------------------- */
/* Les diff�rents types d'�crans */
/* ----------------------------- */

static UBYTE *EcranLabels[] =
{
	(UBYTE *)"Propre",
	(UBYTE *)"Public",
	NULL
};



/* ---------- */
/* Les r�gles */
/* ---------- */

static UBYTE *ReglesLabels[] =
{
	(UBYTE *)"Jeu de la vie",
	(UBYTE *)"Utilisateur",
	NULL
};



/* ------------------- */
/* Le type des Gadgets */
/* ------------------- */

static UWORD ConfGTypes[] =
{
	INTEGER_KIND,
	INTEGER_KIND,
	INTEGER_KIND,
	INTEGER_KIND,
	CYCLE_KIND,
	CHECKBOX_KIND,
	INTEGER_KIND,
	STRING_KIND,
	CYCLE_KIND,
	CYCLE_KIND,
	CYCLE_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND
};



/* ---------------------------------------------------- */
/* D�finition des gadgets de le fen�tre `Configuration` */
/* ---------------------------------------------------- */

static struct NewGadget ConfNGad[] =
{
	102,   9,  51, 14, (UBYTE *)"R�solution",               NULL, GD_Conf_ResolutionH, PLACETEXT_LEFT,  NULL, NULL,
	177,   9,  51, 14, (UBYTE *)"x",                        NULL, GD_Conf_ResolutionV, PLACETEXT_LEFT,  NULL, NULL,
	102,  28,  51, 14, (UBYTE *)"Visible",                  NULL, GD_Conf_VisibleH,    PLACETEXT_LEFT,  NULL, NULL,
	177,  28,  51, 14, (UBYTE *)"x",                        NULL, GD_Conf_VisibleV,    PLACETEXT_LEFT,  NULL, NULL,
	102,  47, 121, 14, (UBYTE *)"Plateau",                  NULL, GD_Conf_TypePlateau, PLACETEXT_LEFT,  NULL, NULL,
	102,  79,  26, 11, (UBYTE *)"Sauvegardes automatiques", NULL, GD_Conf_SauveAuto,   PLACETEXT_RIGHT, NULL, NULL,
	467,  76,  71, 14, (UBYTE *)"Temps",                    NULL, GD_Conf_Temps,       PLACETEXT_LEFT,  NULL, NULL,
	102,  95, 436, 14, (UBYTE *)"R�pertoire",               NULL, GD_Conf_Repertoire,  PLACETEXT_LEFT,  NULL, NULL,
	102, 124, 121, 14, (UBYTE *)"Workbench",                NULL, GD_Conf_WBEcran,     PLACETEXT_LEFT,  NULL, NULL,
	102, 143, 121, 14, (UBYTE *)"Ecran",                    NULL, GD_Conf_Ecran,       PLACETEXT_LEFT,  NULL, NULL,
	102, 172, 144, 14, (UBYTE *)"R�gles",                   NULL, GD_Conf_Regles,      PLACETEXT_LEFT,  NULL, NULL,
	266, 172, 106, 14, (UBYTE *)"D�finition",               NULL, GD_Conf_Definition,  PLACETEXT_IN,    NULL, NULL,
	 44, 215, 106, 14, (UBYTE *)"Sauver",                   NULL, GD_Conf_Sauver,      PLACETEXT_IN,    NULL, NULL,
	190, 215, 106, 14, (UBYTE *)"Utiliser",                 NULL, GD_Conf_Utiliser,    PLACETEXT_IN,    NULL, NULL,
	336, 215, 106, 14, (UBYTE *)"Annuler",                  NULL, GD_Conf_Annuler,     PLACETEXT_IN,    NULL, NULL,
	482, 215, 106, 14, (UBYTE *)"Publicit�",                NULL, GD_Conf_Pub,         PLACETEXT_IN,    NULL, NULL,
	248,  28,  51, 14, (UBYTE *)"Idem",                     NULL, GD_Conf_Idem,        PLACETEXT_IN,    NULL, NULL
};



/* ----------------------------------- */
/* Tags des Gadgets de `Configuration` */
/* ----------------------------------- */

static ULONG ConfGTags[] =
{
	(GTIN_Number), 100, (GTIN_MaxChars), 4, (TAG_DONE),
	(GTIN_Number), 100, (GTIN_MaxChars), 4, (TAG_DONE),
	(GTIN_Number), 100, (GTIN_MaxChars), 4, (TAG_DONE),
	(GTIN_Number), 100, (GTIN_MaxChars), 4, (TAG_DONE),
	(GTCY_Labels), (ULONG)&TypePlateauLabels[ 0 ], (TAG_DONE),
	(TAG_DONE),
	(GTIN_Number), 0, (GTIN_MaxChars), 10, (TAG_DONE),
	(GTST_MaxChars), 256, (TAG_DONE),
	(GTCY_Labels), (ULONG)&WBEcranLabels[ 0 ], (TAG_DONE),
	(GTCY_Labels), (ULONG)&EcranLabels[ 0 ], (TAG_DONE),
	(GTCY_Labels), (ULONG)&ReglesLabels[ 0 ], (TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE)
};




/* -------------------------------------------------- */
/* D�claration, d�finition et ouverture de la fen�tre */
/* -------------------------------------------------- */

int OuvreConf( void )
{
	struct NewGadget ng;
	struct Gadget *g;
	UWORD lc, tc;
	UWORD OffX = Mon_ecran->WBorLeft,
		OffY = Mon_ecran->WBorTop + Mon_ecran->RastPort.TxHeight + 1;


	if ( !( g = CreateContext( &ConfGList ) ) )
		return( 1L );


	for( lc = 0, tc = 0; lc < Conf_CNT; lc++ )
	{
		CopyMem( (char *)&ConfNGad[lc], (char *)&ng, (long)sizeof( struct NewGadget ) );

		ng.ng_VisualInfo = VisualInfo;
		ng.ng_TextAttr   = &Ma_police;
		ng.ng_LeftEdge  += OffX;
		ng.ng_TopEdge   += OffY;

		ConfGadgets[lc] = g = CreateGadgetA( (ULONG)ConfGTypes[lc],
								 g,
								 &ng,
								 (struct TagItem *)&ConfGTags[tc] );

		while( ConfGTags[tc] )
			tc += 2;
		tc++;

		if ( NOT g )
			return( 2L );
	}


	if( !( FenetreConf = OpenWindowTags( NULL,
							 WA_Left,          Config.FenetrePlateauLeft,
							 WA_Top,           Config.FenetrePlateauTop,
							 WA_Width,         640,
							 WA_Height,        234 + OffY,
							 WA_IDCMP,         INTEGERIDCMP|
										 CYCLEIDCMP|
										 CHECKBOXIDCMP|
										 STRINGIDCMP|
										 BUTTONIDCMP|
										 IDCMP_GADGETDOWN|
										 IDCMP_GADGETUP|
										 IDCMP_CLOSEWINDOW|
										 IDCMP_REQVERIFY|
										 IDCMP_REFRESHWINDOW,
							 WA_Flags,         WFLG_DRAGBAR|
										 WFLG_DEPTHGADGET|
										 WFLG_CLOSEGADGET|
										 WFLG_SMART_REFRESH|
										 WFLG_ACTIVATE,
							 WA_Gadgets,       ConfGList,
							 WA_Title,         (UBYTE *)"Configuration",
							 WA_CustomScreen,  Mon_ecran,
							 TAG_DONE ) ) )
		return( 4L );

	GT_RefreshWindow( FenetreConf, NULL );

	DessinsConf();

	/* Pour l'instant, ils ne fonctionnent pas */
	OffGadget( ConfGadgets[GD_Conf_SauveAuto],  FenetreConf, NULL );
	OffGadget( ConfGadgets[GD_Conf_Regles],     FenetreConf, NULL );
	OffGadget( ConfGadgets[GD_Conf_Definition], FenetreConf, NULL );

	return( 0L );
}



/* ------------------------------ */
/* Pour afficher les `BevelBoxes` */
/* ------------------------------ */

void DessinsConf( void )
{
	UWORD OffX, OffY;


	OffX = FenetreConf->BorderLeft;
	OffY = FenetreConf->BorderTop;


	DrawBevelBox( FenetreConf->RPort, OffX + 6, OffY + 167, 618, 24, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetreConf->RPort, OffX + 6, OffY + 119, 618, 43, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetreConf->RPort, OffX + 7, OffY +  71, 617, 43, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetreConf->RPort, OffX + 7, OffY +   4, 617, 62, GT_VisualInfo, VisualInfo, TAG_DONE );
}



/* ---------------------- */
/* Pour fermer la fen�tre */
/* ---------------------- */

void FermeConf( void )
{
	if ( FenetreConf )
	{
		/* On se souvient des cordonn�es de cette fen�tre (�a fait plus fun !) */
		Config.FenetrePlateauLeft = FenetreConf->LeftEdge;
		 Config.FenetrePlateauTop = FenetreConf->TopEdge;

		CloseWindow( FenetreConf );
		FenetreConf = NULL;
	}

	if ( ConfGList )
	{
		FreeGadgets( ConfGList );
		ConfGList = NULL;
	}
}



/* ================================================================================ */
/*                 Initialisation de la fen�tre de configuration				*/
/* ================================================================================ */



void InitConf( void )
{
	/* --------------------------------------------- */
	/* D'abord on met les gadgets aux bonnes valeurs */
	/* --------------------------------------------- */

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_ResolutionH],
				 FenetreConf,
				 NULL,
				 (GTIN_Number), Config.ResolutionH, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_ResolutionV],
				 FenetreConf,
				 NULL,
				 (GTIN_Number), Config.ResolutionV, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_VisibleH],
				 FenetreConf,
				 NULL,
				 (GTIN_Number), Config.VisibleH, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_VisibleV],
				 FenetreConf,
				 NULL,
				 (GTIN_Number), Config.VisibleV, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_TypePlateau],
				 FenetreConf,
				 NULL,
				 (GTCY_Active), Config.Plateau, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_SauveAuto],
				 FenetreConf,
				 NULL,
				 (GTCB_Checked), Config.SauveAuto, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_Temps],
				 FenetreConf,
				 NULL,
				 (GTIN_Number), Config.Temps, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_Repertoire],
				 FenetreConf,
				 NULL,
				 (GTST_String), Config.Repertoire, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_WBEcran],
				 FenetreConf,
				 NULL,
				 (GTCY_Active), Config.WorkBench, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_Ecran],
				 FenetreConf,
				 NULL,
				 (GTCY_Active), Config.Ecran, TAG_END );

	GT_SetGadgetAttrs( ConfGadgets[GD_Conf_Regles],
				 FenetreConf,
				 NULL,
				 (GTCY_Active), Config.Regles, TAG_END );



	/* --------------------------------------------------------------------- */
	/* On initialise les valeurs interm�diaires : l'utilisateur peut changer */
	/* des trucs mais presser `Annuler                                       */
	/* --------------------------------------------------------------------- */

	ValeurModifiee[GD_Conf_ResolutionH] = Config.ResolutionH;

	ValeurModifiee[GD_Conf_ResolutionV] = Config.ResolutionV;

	   ValeurModifiee[GD_Conf_VisibleH] = Config.VisibleH;

	   ValeurModifiee[GD_Conf_VisibleV] = Config.VisibleV;

	ValeurModifiee[GD_Conf_TypePlateau] = Config.Plateau;

	  ValeurModifiee[GD_Conf_SauveAuto] = Config.SauveAuto;

	    ValeurModifiee[GD_Conf_WBEcran] = Config.WorkBench;

	      ValeurModifiee[GD_Conf_Ecran] = Config.Ecran;

	     ValeurModifiee[GD_Conf_Regles] = Config.Regles;
}



/* ================================================================================ */
/*            Transfert des nouvelles valeurs dans la structure Conf			*/
/* ================================================================================ */



void TransfertConf( void )
{
	Config.ResolutionH = ValeurModifiee[GD_Conf_ResolutionH];

	Config.ResolutionV = ValeurModifiee[GD_Conf_ResolutionV];

	   Config.VisibleH = ValeurModifiee[GD_Conf_VisibleH];

	   Config.VisibleV = ValeurModifiee[GD_Conf_VisibleV];

		Config.Plateau = ValeurModifiee[GD_Conf_TypePlateau];

	  Config.SauveAuto = ValeurModifiee[GD_Conf_SauveAuto];

		  Config.Temps = ValeurModifiee[GD_Conf_Temps];

	strcpy( Config.Repertoire, ( (struct StringInfo *)ConfGadgets[GD_Conf_Repertoire]->SpecialInfo )->Buffer );

	  Config.WorkBench = ValeurModifiee[GD_Conf_WBEcran];

		  Config.Ecran = ValeurModifiee[GD_Conf_Ecran];
 
		 Config.Regles = ValeurModifiee[GD_Conf_Regles];
}



/* ================================================================================ */
/*                           Utilise la structure Conf					*/
/* ================================================================================ */



void UtiliseConf( void )
{
	/* ------------------------------ */
	/* Rafraichissement des Scrollers */
	/* ------------------------------ */

	ValeurH = 0;
	GT_SetGadgetAttrs(
				PlateauGadgets[GD_ScrollerH],
				FenetrePlateau,
				NULL,
				GTSC_Top,     ValeurH,
				GTSC_Total,   Config.ResolutionH,
				GTSC_Visible, Config.VisibleH,
				TAG_END	);

	ValeurV = 0;
	GT_SetGadgetAttrs(
				PlateauGadgets[GD_ScrollerV],
				FenetrePlateau,
				NULL,
				GTSC_Top,     ValeurV,
				GTSC_Total,   Config.ResolutionV,
				GTSC_Visible, Config.VisibleV,
				TAG_END	);

	/* ------------------------------ */
	/* Quoi qu'on fait du WorkBench ? */
	/* ------------------------------ */
	if( Config.WorkBench )
		OpenWorkBench();
	else
		CloseWorkBench();



	/* ------------------------------------------ */
	/* Doit-on ouvrir la fen�tre d'informations ? */
	/* ------------------------------------------ */

	if( Config.FenetreInfos )            /* Si on veut le fen�tre d'informations */
		if( !FenetreInfos )            /* Et qu'elle n'est pas ouverte         */
			Informations( FALSE );   /* Alors on l'ouvre                     */
		else                           /* Mais si elle est d�j� ouverte        */
			Informations( TRUE );    /* On met son contenu � jour            */
}



/* ================================================================================ */
/*              Lecture de la configuration ou cr�ation du fichier			*/
/* ================================================================================ */



void LitConf( void )
{
	BPTR File_Handle;
	WORD TailleConf = sizeof( struct Conf );
	LONG NBBytes;

	File_Handle = (BPTR)Open( "ENVARC:VieIII.config", MODE_OLDFILE );
	if( File_Handle != 0 )
	{
		NBBytes = Read( File_Handle, (char *)&Config, TailleConf );
		Close( File_Handle );
		if( NBBytes != TailleConf )
			CleanExit( 15 );
	}
}



/* ================================================================================ */
/*                        Sauvegarde de la configuration					*/
/* ================================================================================ */



void SauveConf( void )
{
	BPTR File_Handle;
	WORD TailleConf = sizeof( struct Conf );
	LONG NBBytes;

	File_Handle = (BPTR)Open( "ENVARC:VieIII.config", MODE_NEWFILE );
	NBBytes = Write( File_Handle, (char *)&Config, TailleConf );
	Close( File_Handle );
	if( NBBytes != TailleConf )
		CleanExit( 6 );

	ConfigModifiee = FALSE;
}



/* ================================================================================ */
/*                          Gestion de la configuration					*/
/* ================================================================================ */



void Configuration( void )
{
	ULONG Classe;            /* Les IDCMPFlags                              */
	APTR Adresse;            /* L'adresse de c'qui nous a envoy� un message */
	UWORD Code;
	UBYTE BP = 0;            /* Le gadget s�lectionn�                       */
	struct IntuiMessage *Le_message;



	if( OuvreConf() > 0 )
		CleanExit( 13 );



	/* ------------------------------------------------------------- */
	/* On transfert les valeurs de la structure Conf vers la fen�tre */
	/* ------------------------------------------------------------- */

	InitConf();



	/* ----------------------------------------------------------------------- */
	/* On attend jusqu'a ce que l'utilisateur ai press� le gadget de fermeture */
	/* ----------------------------------------------------------------------- */

	while( FenetreConf )
	{
		/* --------------------------------------------- */
		/* On attend jusqu'� ce qu'on recoive un message */
		/* --------------------------------------------- */

		WaitPort( FenetreConf->UserPort );

		/* ------------------------------------------------------------------- */
		/* Tant qu'on re�oit bien les messages (si on en avait re�u plusieurs) */
		/* ------------------------------------------------------------------- */

		while(  FenetreConf &&
			    FenetreConf->UserPort &&
			   (Le_message = (struct IntuiMessage *)GT_GetIMsg( FenetreConf->UserPort ) ) )
		{
			/* ------------------------------------------------------------ */
			/* Apr�s avoir collect� avec succ�s un message, on peut le lire */
			/* et l'enregistrer pour traitement ulterieur                   */
			/* ------------------------------------------------------------ */

			  Classe = Le_message->Class;
			    Code = Le_message->Code;
			 Adresse = Le_message->IAddress;



			/* -------------------------------------------------------------------- */
			/* Apr�s avoir lut le message on s'en va aussi vite que possible        */
			/* ATTENTION ! Ne jamais essayer de lire un message apr�s s'�tre repli� */
			/* -------------------------------------------------------------------- */

			GT_ReplyIMsg( Le_message );

			
			
			/* ------------------------------------------ */
			/* On regarde quel message IDCMP a �t� envoy� */
			/* ------------------------------------------ */

			switch( Classe )
			{
			case IDCMP_CLOSEWINDOW:
				SetPointer( FenetreConf, ZzzPointer, 16, 16, 0, 0);
				if( Alerte( FenetreConf,  "Voulez-vous r�ellement quitter Vie III ?", "Oui", "Non" ) )
				{
					if( ConfigModifiee )
					{
						if( Alerte( FenetreConf, "La configuration a �t� modifi�e, quitter ?",
										 "Quitter",
										 "NON !!" ) )
							CleanExit( 0 );
					}
					else
					{
						CleanExit( 0 );
					}
				}
				ClearPointer( FenetreConf );
				break;

			case IDCMP_REFRESHWINDOW:
				GT_BeginRefresh( FenetreConf );
				DessinsConf();
				GT_EndRefresh( FenetreConf, TRUE );
				break;

			case IDCMP_GADGETDOWN:
			case IDCMP_GADGETUP:
				for( BP=0; Adresse != ConfGadgets[BP]; BP++ );

				switch( BP )
				{
				/* ------------------------------------------------------- */
				/* Pour simplifier la saisie et les �ventuelles changement */
				/* des valeurs dans les gadgets de textes, je teste les    */
				/* quatres � chaque fois                                   */
				/* ------------------------------------------------------- */
				case GD_Conf_ResolutionH:
				case GD_Conf_ResolutionV:
				case GD_Conf_VisibleH:
				case GD_Conf_VisibleV:
					/* GD_Conf_ResolutionH */
					ValeurModifiee[GD_Conf_ResolutionH] = atoi( ( (struct StringInfo *)ConfGadgets[GD_Conf_ResolutionH]->SpecialInfo )->Buffer );

					/* GD_Conf_ResolutionV */
					ValeurModifiee[GD_Conf_ResolutionV] = atoi( ( (struct StringInfo *)ConfGadgets[GD_Conf_ResolutionV]->SpecialInfo )->Buffer );

					/* GD_Conf_VisibleH */
					ValeurModifiee[GD_Conf_VisibleH] = atoi( ( (struct StringInfo *)ConfGadgets[GD_Conf_VisibleH]->SpecialInfo )->Buffer );

					ValeurModifiee[GD_Conf_VisibleH] = MIN( ValeurModifiee[GD_Conf_VisibleH], LARGEUR_MAX );
					ValeurModifiee[GD_Conf_VisibleH] = MIN( ValeurModifiee[GD_Conf_VisibleH], ValeurModifiee[GD_Conf_ResolutionH] );

					GT_SetGadgetAttrs( ConfGadgets[GD_Conf_VisibleH],
								 FenetreConf,
								 NULL,
								 (GTIN_Number), ValeurModifiee[GD_Conf_VisibleH], TAG_END );

					/* GD_Conf_VisibleV */
					ValeurModifiee[GD_Conf_VisibleV] = atoi( ( (struct StringInfo *)ConfGadgets[GD_Conf_VisibleV]->SpecialInfo )->Buffer );

					ValeurModifiee[GD_Conf_VisibleV] = MIN( ValeurModifiee[GD_Conf_VisibleV], HAUTEUR_MAX );
					ValeurModifiee[GD_Conf_VisibleV] = MIN( ValeurModifiee[GD_Conf_VisibleV], ValeurModifiee[GD_Conf_ResolutionV] );

					GT_SetGadgetAttrs( ConfGadgets[GD_Conf_VisibleV],
								 FenetreConf,
								 NULL,
								 (GTIN_Number), ValeurModifiee[GD_Conf_VisibleV], TAG_END );
					break;

				case GD_Conf_TypePlateau:
				case GD_Conf_SauveAuto:
				case GD_Conf_WBEcran:
				case GD_Conf_Ecran:
				case GD_Conf_Regles:
					ValeurModifiee[BP] = Code;
					break;

				case GD_Conf_Idem:
					ValeurModifiee[GD_Conf_VisibleH] = ValeurModifiee[GD_Conf_ResolutionH];
					ValeurModifiee[GD_Conf_VisibleH] = MIN( ValeurModifiee[GD_Conf_VisibleH], LARGEUR_MAX );

					GT_SetGadgetAttrs( ConfGadgets[GD_Conf_VisibleH],
								 FenetreConf,
								 NULL,
								 (GTIN_Number), ValeurModifiee[GD_Conf_VisibleH], TAG_END );

					ValeurModifiee[GD_Conf_VisibleV] = ValeurModifiee[GD_Conf_ResolutionV];
					ValeurModifiee[GD_Conf_VisibleV] = MIN( ValeurModifiee[GD_Conf_VisibleV], HAUTEUR_MAX );

					GT_SetGadgetAttrs( ConfGadgets[GD_Conf_VisibleV],
								 FenetreConf,
								 NULL,
								 (GTIN_Number), ValeurModifiee[GD_Conf_VisibleV], TAG_END );

					break;

				case GD_Conf_Sauver:
				case GD_Conf_Utiliser:
					ConfigModifiee = TRUE;
					FermeConf();                                        // On ferme la fen�tre
					if( ValeurModifiee[GD_Conf_Ecran] != Config.Ecran ) // Si on veut changer de type d'�cran
					{
						if( FenetreInfos )                              // Si la fen�tre d'informations est ouverte
							FermeInfos();                               // On la ferme
						FermeEcran();                                   // Et on ferme l'�cran
					}
					TransfertConf();                                    // On transfert les nouvelles valeurs dans la structure Conf
					if( !Mon_ecran )                                    // Si l'�cran n'est pas ouvert
						OuvreEcran();                                   // Et bien on l'ouvre !
					if( BP == GD_Conf_Sauver )                          // Si on veut sauver la configuration
						SauveConf();                                    // Devinez quoi ... on la sauve !
					break;                                              // Hop ! fini !

				case GD_Conf_Annuler:
					FermeConf();
					break;

				case GD_Conf_Pub:
					Publicite();
					break;
				}
				break;

			}   /* Fin du `switch( Classe )` */

		}   /* Fin du `while( Le_message = ...` */

	}   /* Fin du `while( FenetreConf )` */
}
