#ifndef CONSOLE_H
#define CONSOLE_H 1

extern struct Window *FenetreControle;

int  OuvreControle( void );     /* Pour ouvrir le panneau de controle      */
void DessinsControle( void );   /* Les dessins du panneau                  */
void FermeControle( void );     /* Pour le fermer                          */
void Controle( void );          /* La fen�tre de cont�le des Cellules & Co */
void FantomizeControle( BOOL Shadow );

#endif

