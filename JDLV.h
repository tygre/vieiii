#ifndef JDLV_H
#define JDLV_H 1

extern BOOL  **AdrTab;
extern BOOL  **AdrTab1;
extern struct Remember *Memoire;
extern struct Remember *Memoire1;

extern UWORD  *AdrX;
extern UWORD  *AdrY;
extern UWORD  *AdrX1;
extern UWORD  *AdrY1;

extern UWORD   ValeurH, ValeurV;       /* Les valeurs de chaque Scroller              */

#define CELLULE(x,y)   *(*(AdrTab + (y)) + (x))
#define CELLULE1(x,y)  *(*(AdrTab1 + (y)) + (x))
#define CCX(n)         *(AdrX + (n))
#define CCY(n)         *(AdrY + (n))
#define CCX1(n)        *(AdrX1 + (n))
#define CCY1(n)        *(AdrY1 + (n))

void AlloueTableaux( BOOL Nouveau );   /* Pour allouer la m�moire pour le plateau       */
void PauseCellule( void );             /* Se charge de pauser et d'enlever les Cellules */
void AfficheCellules( void );          /* Pour afficher les Cellules                    */
void JDLV( void );                     /* Le jeu de la vie classique                    */
void TraceCellule( UWORD XCase,
			       UWORD YCase,
			       UBYTE Couleur );    /* Juste pour afficher une Cellule               */
void EffaceTableaux( void );

#endif


