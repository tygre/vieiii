/* ================================================================================ */
/*                                                                                  */
/* Vie III, la nouvelle g�n�ration                                                  */
/*		Programm� par Tygre                                                         */
/*                                                                                  */
/* ================================================================================ */
/*                                                                                  */
/* Les chargements/sauvegardes, revision 0.7.2                                      */
/*                                                                                  */
/* ================================================================================ */



#include <clib/asl_protos.h>
#include <clib/dos_protos.h>
#include <clib/exec_protos.h>
#include <exec/types.h>
#include <intuition/screens.h>
#include <libraries/asl.h>
#include <utility/tagitem.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Interface.h"
#include "JDLV.h"
#include "Plateau.h"
#include "VieIII.h"

#include "Fortify.h"



#define MIN(x,y)     ((x)<(y) ? (x):(y))
#define MAX(x,y)     ((x)>(y) ? (x):(y))
#define TAILLE_LIGNE 1024



extern struct Conf    Config;
extern int            NBCels;
extern struct Screen *Mon_ecran;



/* ================================================================================ */
/*                           D�claration des varaibles					*/
/* ================================================================================ */



char FichierCharger[256];          // L� o� sera le nom du fichier
struct FileRequester *FR;          // Pour le s�lecteur de fichier
int OffsetX = 0, OffsetY = 0;      // Les offsets de chargement




/* ================================================================================ */
/*         D�finition de ma fonction pour avoir un s�lecteur de fichier			*/
/* ================================================================================ */



APTR Selecteur( Sauvegarde, Titre, OK_Texte, Repertoire )

BOOL Sauvegarde;
char *Titre, *OK_Texte, *Repertoire;

{
	return( AllocAslRequestTags( ASL_FileRequest,
					     ASLFR_Screen,          Mon_ecran,
					     ASLFR_DoPatterns,      TRUE,
					     ASLFR_DoSaveMode,      Sauvegarde,
					     ASLFR_RejectIcons,     TRUE,
					     ASLFR_InitialDrawer,   Repertoire,
					     ASLFR_InitialLeftEdge, 160,
					     ASLFR_InitialTopEdge,   23,
					     ASLFR_InitialWidth,    300,
					     ASLFR_TitleText,       Titre,
					     ASLFR_PositiveText,    OK_Texte,
					     ASLFR_NegativeText,    "Annuler",
					     ASLFR_InitialPattern,  "#?",
					     ASLFR_SleepWindow,     TRUE,
					     TAG_DONE ) );
}



/* ================================================================================ */
/*                La fonction pour lire une ligne dans le fichier				*/
/* ================================================================================ */



int LireLigne( FilePointer, ChaineDest )

FILE *FilePointer;   // Un pointeur sur le fichier dans lequel lire la ligne
char *ChaineDest;    // Un pointeur sur la chaine dans laquelle sera copi� la ligne

{
	char a = '\0';
	int  i = 0;

	a = fgetc( FilePointer );
	while( i < ( TAILLE_LIGNE - 1 ) &&
		   a != 10 &&
		   !feof( FilePointer ) )
	{
		ChaineDest[i] = a;
		i++;
		a = fgetc( FilePointer );
	}
	ChaineDest[i] = '\0';

	return( i );   // On retourne le nombre de charact�res lus
}



/* ================================================================================ */
/*                      La fonction pour charger un fichier					*/
/* ================================================================================ */



void Charge( void )
{
	FILE *FilePointer;
	char LigneLue[TAILLE_LIGNE], TypeData[15];
	int NBLu;
	int X, Y;
	BOOL Depassement;
	int MinX, MinY, MaxX, MaxY;

	TypeData[0] = '\0';
	Depassement = FALSE;
	MinX = 0;
	MinY = 0;
	MaxX = 0;
	MaxY = 0;

	if( FR = (struct FileRequester *)Selecteur( FALSE, "Charger une population", "Charger", Config.Repertoire ) )
	{
		BOOL Choix = AslRequest( FR, NULL );

		if( Choix  && FR->fr_File != NULL )
		{
			/* ---------------------------------------------------- */
			/* On regarde si le nom du tiroir se termine par un '/' */
			/* ---------------------------------------------------- */

			sprintf( FichierCharger, "%s", FR->fr_Drawer );

			if( FichierCharger[ strlen( FichierCharger )-1 ] == '/' ||
			    FichierCharger[ strlen( FichierCharger )-1 ] == ':' ||
			    FichierCharger == NULL )
				sprintf( FichierCharger, "%s%s", FR->fr_Drawer, FR->fr_File );
			else
				sprintf( FichierCharger, "%s/%s", FR->fr_Drawer, FR->fr_File );

			/* ------------------------------- */
			/* On met � jour le tiroir courant */
			/* ------------------------------- */

			strcpy( Config.Repertoire, FR->fr_Drawer );

			/* ----------------------------- */
			/* C'est parti pour la lecture ! */
			/* ----------------------------- */

			if( ( FilePointer = fopen( FichierCharger ,"r" ) ) != NULL )
			{
				AlloueTableaux( TRUE );

				NBCels = 0;

				/* -------------------------------------------------------- */
				/* On lit la premi�re ligne (qui peut contenir les offsets) */
				/* -------------------------------------------------------- */

				NBLu = LireLigne( FilePointer, LigneLue );

				if( !NBLu )
				{
					Alerte( FenetrePlateau, "Fichier vide |?", "D'accord", "Compris" );
				}
				else
				{
					sscanf( LigneLue, "%s%d%d", TypeData, &OffsetX, &OffsetY );
				}

				if( strcmp( TypeData, "#P" ) == 0 )   // Si les deux chaines sont �gales strcmp() egale 0
				{

					/* ---------------------------------------- */
					/*							                */
					/* Cas o� le fichier est de type `Picture`  */
					/* Les donn�es se pr�sentent sous la forme  */
					/* des offsets puis de lignes ressemblant � */
					/*							                */
					/*   ....*..***.**                          */
					/*   ...**.****.**                          */
					/* Dans lesquelles * repr�sente une Cellule */
					/*							                */
					/* ---------------------------------------- */

					OffsetX = 0;
					OffsetY = 0;

					NBLu = LireLigne( FilePointer, LigneLue );

					// Ligne par ligne...
					Y = 0;
					while( NBLu )
					{
						// Charact�re par charact�re...
						X = 0;
						while( X < NBLu )
						{
							if( LigneLue[X] == '*' )
							{
								if( X >= 0 && Y >= 0 && X < Config.ResolutionH && Y < Config.ResolutionV )
								{
									CELLULE( X, Y ) = TRUE;
									CCX( NBCels ) = X;
									CCY( NBCels ) = Y;
									NBCels++;
								}
								else
								{
									Depassement = TRUE;
									MinX = MIN( MinX, X );
									MinY = MIN( MinY, Y );
									MaxX = MAX( MaxX, X );
									MaxY = MAX( MaxY, Y );
								}
							}
							
							// Charact�re suivant
							X++;
						}
						
						// Ligne suivante
						NBLu = LireLigne( FilePointer, LigneLue );
						Y++;
					}

				}
				else if( strcmp( TypeData, "#R" ) == 0 )   // Si les deux chaines sont �gales strcmp() egale 0
				{
					/* ------------------------------------------------------------- */
					/* On est dans le cas relatif, les Offsets doivent �tre utilis�s */
					/* ------------------------------------------------------------- */

					OffsetX = Config.ResolutionH / 2;
					OffsetY = Config.ResolutionV / 2;

					NBLu = LireLigne( FilePointer, LigneLue );

					while( NBLu )
					{
						sscanf( LigneLue, "%d%d", &X, &Y );
						X += OffsetX;
						Y += OffsetY;
						if( X >= 0 && Y >= 0 && X < Config.ResolutionH && Y < Config.ResolutionV )
						{
							CELLULE( X, Y ) = TRUE;
							CCX( NBCels ) = X;
							CCY( NBCels ) = Y;
							NBCels++;
						}
						else
						{
							Depassement = TRUE;
							MinX = MIN( MinX, X );
							MinY = MIN( MinY, Y );
							MaxX = MAX( MaxX, X );
							MaxY = MAX( MaxY, Y );
						}
						NBLu = LireLigne( FilePointer, LigneLue );
					}
				}
				else
				{

					OffsetX = 0;
					OffsetY = 0;

					if( isdigit( TypeData[0] ) )   // Si c'est un chifre, il n'y avait pas d'en-t�te
					{
						/* ---------------------------------------------------------------------------- */
						/* Dans ce cas `TypeData` va contenir la coordonn�e en X de la premi�re Cellule */
						/* et OffsetX celle en Y                            							*/
						/* ---------------------------------------------------------------------------- */

						sscanf( TypeData, "%d", &X );
						Y = OffsetX;

						if( X >= 0 && Y >= 0 && X < Config.ResolutionH && Y < Config.ResolutionV )
						{
							CELLULE( X, Y ) = TRUE;
							CCX( NBCels ) = X;
							CCY( NBCels ) = Y;
							NBCels++;
						}
						else
						{
							Depassement = TRUE;
							MinX = MIN( MinX, X );
							MinY = MIN( MinY, Y );
							MaxX = MAX( MaxX, X );
							MaxY = MAX( MaxY, Y );
						}
					}

					NBLu = LireLigne( FilePointer, LigneLue );

					while( NBLu )
					{
						sscanf( LigneLue, "%d%d", &X, &Y );
						if( X >= 0 && Y >= 0 && X < Config.ResolutionH && Y < Config.ResolutionV )
						{
							CELLULE( X, Y ) = TRUE;
							CCX( NBCels ) = X;
							CCY( NBCels ) = Y;
							NBCels++;
						}
						else
						{
							Depassement = TRUE;
							MinX = MIN( MinX, X );
							MinY = MIN( MinY, Y );
							MaxX = MAX( MaxX, X );
							MaxY = MAX( MaxY, Y );
						}
						NBLu = LireLigne( FilePointer, LigneLue );
					}

				}

				if( Depassement )
				{
					LigneLue[0] = '\0';

					sprintf( LigneLue, "Certaines cellules d�passe du plateau aux coordonn�es %dx%d et %dx%d", MinX, MinY, MaxX, MaxY );

					Alerte( FenetrePlateau, LigneLue, "D'accord", "Compris" );
				}

				AfficheCellules();

				/* ------------------- */
				/* On ferme le fichier */
				/* ------------------- */

				fclose( FilePointer );

				if( NBCels > 0 )
					Fantomize( FALSE );
				else
					Fantomize( TRUE );

			}

		}   // Fin du `if( Choix  && FR ...`

		FreeAslRequest( FR );
	}
}



/* ================================================================================ */
/*                      La fonction pour sauver un fichier					*/
/* ================================================================================ */



void Sauve( SauverComme )

UBYTE SauverComme;

{
	if( strlen( FichierCharger ) == 0 || SauverComme == TRUE )
	{
		if( FR = (struct FileRequester *)Selecteur( TRUE, "Sauver une population", "Sauver", Config.Repertoire ) )
		{
			BOOL Choix = AslRequest( FR, NULL );
			if( Choix  && FR->fr_File != NULL )
			{
				/* ---------------------------------------------------- */
				/* On regarde si le nom du tiroir se termine par un '/' */
				/* ---------------------------------------------------- */

				sprintf( FichierCharger, "%s", FR->fr_Drawer );

				if( FichierCharger[ strlen( FichierCharger )-1 ] == '/' ||
				    FichierCharger[ strlen( FichierCharger )-1 ] == ':' ||
				    FichierCharger == NULL )
					sprintf( FichierCharger, "%s%s", FR->fr_Drawer, FR->fr_File );
				else
					sprintf( FichierCharger, "%s/%s", FR->fr_Drawer, FR->fr_File );

				/* ------------------------------- */
				/* On met � jour le tiroir courant */
				/* ------------------------------- */

				strcpy( Config.Repertoire, FR->fr_Drawer );

			}
			FreeAslRequest( FR );
		}
	}

	EcrireFichier();
}


/* ================================================================================ */
/*                      Fonction pour �crire le fichier					*/
/* ================================================================================ */



void EcrireFichier(void)
{
	BPTR FilePointer;
	register unsigned int NB;
	char Ligne[100];

	if( ( FilePointer = Open( FichierCharger ,MODE_NEWFILE ) ) != 0 )
	{
		/* Le type de fichier life */
		Write( FilePointer, "#A\n", 3 );

		for( NB = 0; NB < NBCels; NB++ )
		{
			sprintf( Ligne, "%d %d\n", CCX( NB )-OffsetX, CCY( NB )-OffsetY );
			Write( FilePointer, Ligne, strlen( Ligne ) );
		}
		Close( FilePointer );
	}
}
