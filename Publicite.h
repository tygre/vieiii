#ifndef PUBLICITE_H
#define PUBLICITE_H 1

extern struct Window  *FenetrePub;   /* L'adresse de la fen�tre */

int  OuvrePub( void );    /* Pour ouvrir et initialiser la fen�tre `Publicit�` */
void FermePub( void );    /* Pour fermer la fen�tre `Publicit�`                */
void Publicite( void );   /* Pour g�rer la pub                                 */

#endif


