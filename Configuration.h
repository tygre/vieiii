#ifndef CONFIGURATION_H
#define CONFIGURATION_H 1

extern struct Window *FenetreConf;

int  OuvreConf( void );       /* Pour ouvrir la fen�tre de configuration                 */
void DessinsConf( void );     /* Les dessins de la fen�tre `Configuration`               */
void FermeConf( void );       /* Pour fermer la fen�tre `Configuration`                  */
void InitConf( void );        /* Pour initialiser la configuration                       */
void TransfertConf( void );   /* Transfert les nouvelles valeurs dans la structures Conf */
void UtiliseConf( void );     /* Pour se servir des valeurs de la structure Conf         */
void Configuration( void );   /* Pour g�rer la Configuration                             */
void LitConf( void );         /* Lecture ou cr�ation du fichier .config                  */
void SauveConf( void );       /* Sauvegadre de la configuration                          */

#endif

