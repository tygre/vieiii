#ifndef INFORMATIONS_H
#define INFORMATIONS_H 1

extern struct Window *FenetreInfos;

int  OuvreInfos( void );              /* Pour ouvrir la fen�tre d'informations */
void FermeInfos( void );              /* Pour la fermer                        */
void Informations( BOOL Modifier );   /* Pour la g�rer                         */

#endif

