/* ================================================================================ */
/*                                                                                  */
/* Vie III, la nouvelle g�n�ration                                                  */
/*		Programm� par Tygre                                                         */
/*                                                                                  */
/* ================================================================================ */
/*                                                                                  */
/* La publicit�, revision 0.7.1                                                     */
/*                                                                                  */
/* ================================================================================ */



#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/intuition_protos.h>
#include <exec/nodes.h>
#include <exec/lists.h>
#include <libraries/gadtools.h>
#include <utility/tagitem.h>

#include "VieIII.h"

#include "Fortify.h"



#define ConfRev     "0.9.1"
#define ControleRev "0.5.1"
#define InfosRev    "0.8.1"
#define InterRev    "0.7.1"
#define JDLVRev     "0.15.1"
#define PlateauRev  "0.3.1"
#define PubRev      "0.7.1"

#define GD_Pub_ListView   0

#define VERSION     "0.19"
#define SASCVER     "6.51"
#define GADTOOLSVER "1.4"



extern struct TextAttr Ma_police;
extern struct Screen  *Mon_ecran;
extern APTR VisualInfo;



/* ================================================================================ */
/*                           D�claration des varaibles					*/
/* ================================================================================ */



struct Window  *FenetrePub;   /* L'adresse de la fen�tre */

static struct Gadget  *PubGList;
static struct Gadget  *PubGadget;
static struct MinList  GadgetList;



/* ================================================================================ */
/*                    D�finition de la fen�tre `Informations`				*/
/* ================================================================================ */



/* ----------------------- */
/* Le texte de la ListView */
/* ----------------------- */

static struct Node GadgetNodes[] =
{
	&GadgetNodes[1], (struct Node *)&GadgetList.mlh_Head,   0, 0, "            Vie III",
	&GadgetNodes[2],                      &GadgetNodes[0],  0, 0, "            ~~~~~~~",
	&GadgetNodes[3],                      &GadgetNodes[1],  0, 0, "         Version "VERSION,
	&GadgetNodes[4],                      &GadgetNodes[2],  0, 0, "",
	&GadgetNodes[5],                      &GadgetNodes[3],  0, 0, "           Par Tygre",
	&GadgetNodes[6],                      &GadgetNodes[4],  0, 0, "",
	&GadgetNodes[7],                      &GadgetNodes[5],  0, 0, "      -------------------",
	&GadgetNodes[8],                      &GadgetNodes[6],  0, 0, "",
	&GadgetNodes[9],                      &GadgetNodes[7],  0, 0, "    Version de SAS/C v"SASCVER,
	&GadgetNodes[10],                     &GadgetNodes[8],  0, 0, "    Version de VBCC  v0.8j",
	&GadgetNodes[11],                     &GadgetNodes[9],  0, 0, "       Interface version",
	&GadgetNodes[12],                     &GadgetNodes[10], 0, 0, "             "GADTOOLSVER,
	&GadgetNodes[13],                     &GadgetNodes[11], 0, 0, "       avec GadToolsBox",
	&GadgetNodes[14],                     &GadgetNodes[12], 0, 0, "",
	&GadgetNodes[15],                     &GadgetNodes[13], 0, 0, "      -------------------",
	&GadgetNodes[16],                     &GadgetNodes[14], 0, 0, "",
	&GadgetNodes[17],                     &GadgetNodes[15], 0, 0, "  Ce logiciel n'est pas dans",
	&GadgetNodes[18],                     &GadgetNodes[16], 0, 0, "       le Domaine Public",
	&GadgetNodes[19],                     &GadgetNodes[17], 0, 0, "    Cependant il peut �tre",
	&GadgetNodes[20],                     &GadgetNodes[18], 0, 0, "      distribu� librement",
	&GadgetNodes[21],                     &GadgetNodes[19], 0, 0, "  tant qu'il ne subit AUCUNES",
	&GadgetNodes[22],                     &GadgetNodes[20], 0, 0, "  modifications et est donn�",
	&GadgetNodes[23],                     &GadgetNodes[21], 0, 0, "  sans AUCUNES contre-parties",
	&GadgetNodes[24],                     &GadgetNodes[22], 0, 0, "    (financi�res ou autres)",
	&GadgetNodes[25],                     &GadgetNodes[23], 0, 0, "",
	&GadgetNodes[26],                     &GadgetNodes[24], 0, 0, "       Je d�cline TOUTE",
	&GadgetNodes[27],                     &GadgetNodes[25], 0, 0, "  responsabilit� des dommages",
	&GadgetNodes[28],                     &GadgetNodes[26], 0, 0, "      pouvant resulter de",
	&GadgetNodes[29],                     &GadgetNodes[27], 0, 0, "       l'utilisation de",
	&GadgetNodes[30],                     &GadgetNodes[28], 0, 0, "          ce logiciel",
	&GadgetNodes[31],                     &GadgetNodes[29], 0, 0, "",
	&GadgetNodes[32],                     &GadgetNodes[30], 0, 0, "   VOUS UTILISEZ CE LOGICIEL",
	&GadgetNodes[33],                     &GadgetNodes[31], 0, 0, "   A VOS RISQUES ET PERILS !",
	&GadgetNodes[34],                     &GadgetNodes[32], 0, 0, "",
	&GadgetNodes[35],                     &GadgetNodes[33], 0, 0, "   Cependant vous ne devriez",
	&GadgetNodes[36],                     &GadgetNodes[34], 0, 0, "  pas avoir de probl�mes ...",
	&GadgetNodes[37],                     &GadgetNodes[35], 0, 0, "",
	&GadgetNodes[38],                     &GadgetNodes[36], 0, 0, "      -------------------",
	&GadgetNodes[39],                     &GadgetNodes[37], 0, 0, "",
	&GadgetNodes[40],                     &GadgetNodes[38], 0, 0, "       Pour me contacter",
	&GadgetNodes[41],                     &GadgetNodes[39], 0, 0, "",
	&GadgetNodes[42],                     &GadgetNodes[40], 0, 0, "            Tygre",
	&GadgetNodes[43],                     &GadgetNodes[41], 0, 0, "       tygre@chingu.asia",
	&GadgetNodes[44],                     &GadgetNodes[42], 0, 0, "           Montr�al",
	&GadgetNodes[45],                     &GadgetNodes[43], 0, 0, "            Canada",
	&GadgetNodes[46],                     &GadgetNodes[44], 0, 0, "",
	&GadgetNodes[47],                     &GadgetNodes[45], 0, 0, "      -------------------",
	&GadgetNodes[48],                     &GadgetNodes[46], 0, 0, "",
	&GadgetNodes[49],                     &GadgetNodes[47], 0, 0, "        Remerciements �",
	&GadgetNodes[50],                     &GadgetNodes[48], 0, 0, "",
	&GadgetNodes[51],                     &GadgetNodes[49], 0, 0, "      Fran�ois (Kuquami)",
	&GadgetNodes[52],                     &GadgetNodes[50], 0, 0, "     pour ses explications",
	&GadgetNodes[53],                     &GadgetNodes[51], 0, 0, "     et son id�e premi�re",
	&GadgetNodes[54],                     &GadgetNodes[52], 0, 0, "",
	&GadgetNodes[55],                     &GadgetNodes[53], 0, 0, "            Gwena�l",
	&GadgetNodes[56],                     &GadgetNodes[54], 0, 0, "  pour son aide inestimable",
	&GadgetNodes[57],                     &GadgetNodes[55], 0, 0, "      et ses algorithmes",
	&GadgetNodes[58],                     &GadgetNodes[46], 0, 0, "",
	&GadgetNodes[59],                     &GadgetNodes[57], 0, 0, "           Aran Cox",
	&GadgetNodes[60],                     &GadgetNodes[58], 0, 0, "         Stefan Becker",
	&GadgetNodes[61],                     &GadgetNodes[59], 0, 0, "         Matti Rintala",
	&GadgetNodes[62],                     &GadgetNodes[60], 0, 0, "         Tomas Rokicki",
	&GadgetNodes[63],                     &GadgetNodes[61], 0, 0, "              ...",
	&GadgetNodes[64],                     &GadgetNodes[62], 0, 0, "       pour leur sources",
	&GadgetNodes[65],                     &GadgetNodes[63], 0, 0, "",
	&GadgetNodes[66],                     &GadgetNodes[64], 0, 0, "      -------------------",
	&GadgetNodes[67],                     &GadgetNodes[65], 0, 0, "",
	&GadgetNodes[68],                     &GadgetNodes[66], 0, 0, "           Coucou �",
	&GadgetNodes[69],                     &GadgetNodes[67], 0, 0, "",
	&GadgetNodes[70],                     &GadgetNodes[68], 0, 0, "            Gwena�l",
	&GadgetNodes[71],                     &GadgetNodes[69], 0, 0, "      Christophe (Dolby)",
	&GadgetNodes[72],                     &GadgetNodes[70], 0, 0, "     S�bastien (Tune TNT)",
	&GadgetNodes[73],                     &GadgetNodes[71], 0, 0, "             Ludo",
	&GadgetNodes[74],                     &GadgetNodes[72], 0, 0, "            Pascal",
	&GadgetNodes[75],                     &GadgetNodes[73], 0, 0, "         Jean-Pierre",
	&GadgetNodes[76],                     &GadgetNodes[74], 0, 0, "            Armel",
	&GadgetNodes[77],                     &GadgetNodes[75], 0, 0, "            Migel",
	&GadgetNodes[78],                     &GadgetNodes[76], 0, 0, "",
	&GadgetNodes[79],                     &GadgetNodes[77], 0, 0, "       et les autres ...",
	&GadgetNodes[80],                     &GadgetNodes[78], 0, 0, "",
	&GadgetNodes[81],                     &GadgetNodes[79], 0, 0, "      -------------------",
	&GadgetNodes[82],                     &GadgetNodes[80], 0, 0, "",
	&GadgetNodes[83],                     &GadgetNodes[81], 0, 0, "           R�visions",
	&GadgetNodes[84],                     &GadgetNodes[82], 0, 0, "",
	&GadgetNodes[85],                     &GadgetNodes[83], 0, 0, "         Publicite "PubRev,
	&GadgetNodes[86],                     &GadgetNodes[84], 0, 0, "           Plateau "PlateauRev,
	&GadgetNodes[87],                     &GadgetNodes[85], 0, 0, "      Informations "InfosRev,
	&GadgetNodes[88],                     &GadgetNodes[86], 0, 0, "     Configuration "ConfRev,
	&GadgetNodes[89],                     &GadgetNodes[87], 0, 0, "          Contr�le "ControleRev,
	&GadgetNodes[90],                     &GadgetNodes[88], 0, 0, "     Jeu de la vie "JDLVRev,
	&GadgetNodes[91],                     &GadgetNodes[89], 0, 0, "   Entr�es/Sorties "InterRev,
	(struct Node *)&GadgetList.mlh_Tail,  &GadgetNodes[90], 0, 0, ""
};

static struct MinList GadgetList =
{
	(struct MinNode *)&GadgetNodes[0], (struct MinNode *)NULL, (struct MinNode *)&GadgetNodes[89]
};



/* -------------------- */
/* D�finition du gadget */
/* -------------------- */

static struct NewGadget PubNGad[] =
{
	4, 2, 264, 108, NULL, NULL, GD_Pub_ListView, 0 ,NULL, NULL
};



/* -------------------------------------------------- */
/* D�claration, d�finition et ouverture de la fen�tre */
/* -------------------------------------------------- */

int OuvrePub( void )
{
	struct NewGadget ng;
	struct Gadget *g;
	UWORD OffX = Mon_ecran->WBorLeft,
		OffY = Mon_ecran->WBorTop + Mon_ecran->RastPort.TxHeight + 1;

	if( ! ( g = CreateContext( &PubGList ) ) )
		return( 1L );

	CopyMem( (char *)&PubNGad[0], (char *)&ng, (long)sizeof( struct NewGadget ) );

	ng.ng_VisualInfo = VisualInfo;
	ng.ng_TextAttr   = &Ma_police;
	ng.ng_LeftEdge  += OffX;
	ng.ng_TopEdge   += OffY;

	PubGadget = g = CreateGadget( LISTVIEW_KIND,
						g,
						&ng,
						(GTLV_Labels), (ULONG)&GadgetList,
						(GTLV_ReadOnly), TRUE,
						(TAG_DONE) );

	if( NOT g )
		return( 2L );

	if( !( FenetrePub = OpenWindowTags( NULL,
							WA_Left,                 180,
							WA_Top,                   50,
							WA_Width,                280,
							WA_Height,        114 + OffY,
							WA_IDCMP,         LISTVIEWIDCMP|
										IDCMP_CLOSEWINDOW|
										IDCMP_REFRESHWINDOW,
							WA_Flags,         WFLG_DRAGBAR|
										WFLG_DEPTHGADGET|
										WFLG_CLOSEGADGET|
										WFLG_SMART_REFRESH|
										WFLG_ACTIVATE,
							WA_Gadgets,       PubGList,
							WA_Title,         "Publicit�",
							WA_CustomScreen,  Mon_ecran,
							TAG_DONE ) ) )
		return( 4L );

	GT_RefreshWindow( FenetrePub, NULL );

	return( 0L );
}



/* ---------------------- */
/* Pour fermer la fen�tre */
/* ---------------------- */

void FermePub( void )
{
	if( FenetrePub )
	{
		CloseWindow( FenetrePub );
		FenetrePub = NULL;
	}

	if( PubGList )
	{
		FreeGadgets( PubGList );
		PubGList = NULL;
	}
}



/* ================================================================================ */
/*                            Gestion de la ListView						*/
/* ================================================================================ */



void Publicite( void )
{
	BOOL Quitter = FALSE;       /* Pour savoir si on veut s'en aller              */
	ULONG Classe;               /* Les IDCMPFlags                                 */
	struct IntuiMessage *Le_message;



	if( OuvrePub() != 0 )
		CleanExit(4);



	while( Quitter == FALSE )
	{
		/* --------------------------------------------- */
		/* On attend jusqu'� ce qu'on recoive un message */
		/* --------------------------------------------- */

		WaitPort( FenetrePub->UserPort );



		/* ------------------------------------------------------------------- */
		/* Tant qu'on re�oit bien les messages (si on en avait re�u plusieurs) */
		/* ------------------------------------------------------------------- */

		while( Le_message = (struct IntuiMessage *)GT_GetIMsg( FenetrePub->UserPort ) )
		{

			/* ------------------------------------------------------------ */
			/* Apr�s avoir collect� avec succ�s un message, on peut le lire */
			/* et l'enregistrer pour traitement ulterieur                   */
			/* ------------------------------------------------------------ */

			 Classe = Le_message->Class;



			/* ------------------------------------------------------------- */
			/* Apr�s avoir lut le message on s'en va aussi vite que possible */
			/* ATTENTION ! Ne jamais essayer de lire un message apr�s �a     */
			/* ------------------------------------------------------------- */

			GT_ReplyIMsg( Le_message );



			/* ------------------------------------------ */
			/* On regarde quel message IDCMP a �t� envoy� */
			/* ------------------------------------------ */

			switch( Classe )
			{
			case IDCMP_CLOSEWINDOW:
				/* Gadget de fermeture selectionn� */
				Quitter=TRUE;
				break;
			}
		}
	}



	FermePub();
}
