/* ================================================================================ */
/*														                            */
/* Vie III, la nouvelle g�n�ration								                	*/
/*		Programm� par Tygre              							                */
/*														                            */
/* ================================================================================ */
/*														                            */
/* La configuration, revision 0.3.2                                               	*/
/*														                            */
/* ================================================================================ */



#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/intuition_protos.h>
#include <graphics/text.h>
#include <intuition/gadgetclass.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>
#include <utility/tagitem.h>
#include <libraries/gadtools.h>

#include "Plateau.h"
#include "VieIII.h"

#include "Fortify.h"



extern struct Conf Config;
extern int NBCels;
extern struct Screen  *Mon_ecran;
extern APTR VisualInfo;
extern struct TextAttr Ma_police;



/* ================================================================================ */
/*                           D�claration des varaibles					*/
/* ================================================================================ */



struct Window *FenetrePlateau;                /* L'adresse de la fen�tre */
struct Gadget *PlateauGadgets[Plateau_CNT];   /* L'adresse des gadgets   */
static struct Gadget *PlateauGList;
UWORD OffX, OffY;                      /* Les offsets de la fen�tre `Plateau` */



/* ================================================================================ */
/*                      D�finition de la fen�tre `Plateau`					*/
/* ================================================================================ */



/* ------------------------------------------- */
/* Le type des Gadgets de la fen�tre `Plateau` */
/* ------------------------------------------- */

static UWORD PlateauGTypes[] =
{
	SCROLLER_KIND,
	SCROLLER_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND,
	BUTTON_KIND
};



/* ---------------------------------------------- */
/* D�finition des gadgets de le fen�tre `Plateau` */
/* ---------------------------------------------- */

static struct NewGadget PlateauNGad[] =
{
	 41, 216, 405,   9, NULL,                     NULL, GD_ScrollerH, 0            ,NULL, NULL,
	 13,   8,  17, 203, NULL,                     NULL, GD_ScrollerV, 0            ,NULL, NULL,
	478,  14, 121,  14, (UBYTE *)"Nouveau",       NULL, GD_Nouveau,   PLACETEXT_IN ,NULL, NULL,
	478,  33, 121,  14, (UBYTE *)"Charger",       NULL, GD_Charger,   PLACETEXT_IN ,NULL, NULL,
	478,  52, 121,  14, (UBYTE *)"Sauver",        NULL, GD_Sauver,    PLACETEXT_IN ,NULL, NULL,
	478,  71, 121,  14, (UBYTE *)"Sauver comme",  NULL, GD_SauverC,   PLACETEXT_IN ,NULL, NULL,
	478,  90, 121,  14, (UBYTE *)"Imprimer",      NULL, GD_Imprimer,  PLACETEXT_IN ,NULL, NULL,
	478, 119, 121,  14, (UBYTE *)"Vivre",         NULL, GD_Vivre,     PLACETEXT_IN ,NULL, NULL,
	478, 148, 121,  14, (UBYTE *)"Configuration", NULL, GD_Conf,      PLACETEXT_IN ,NULL, NULL,
	478, 177, 121,  14, (UBYTE *)"Informations",  NULL, GD_Infos,     PLACETEXT_IN ,NULL, NULL,
	478, 206, 121,  14, (UBYTE *)"Quitter",       NULL, GD_Quitter,   PLACETEXT_IN ,NULL, NULL
};



/* ----------------------------- */
/* Tags des Gadgets de `Plateau` */
/* ----------------------------- */

static ULONG PlateauGTags[] =
{
	(GTSC_Total), 100, (GTSC_Visible), 100, (GTSC_Arrows), 16, (PGA_Freedom), LORIENT_HORIZ, (GA_RelVerify), TRUE, (TAG_DONE),
	(GTSC_Total), 100, (GTSC_Visible), 100, (GTSC_Arrows),  8, (PGA_Freedom), LORIENT_VERT,  (GA_RelVerify), TRUE, (TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE),
	(TAG_DONE)
};



/* -------------------------------------------------- */
/* D�claration, d�finition et ouverture de la fen�tre */
/* -------------------------------------------------- */

int OuvrePlateau( void )
{
	struct NewGadget ng;
	struct Gadget *g;
	UWORD lc, tc;

	OffX = Mon_ecran->WBorLeft,
	OffY = Mon_ecran->WBorTop + Mon_ecran->RastPort.TxHeight + 1;

	if( ! ( g = CreateContext( &PlateauGList )))
		return( 1L );


	for( lc = 0, tc = 0; lc < Plateau_CNT; lc++ )
	{

		CopyMem((char * )&PlateauNGad[ lc ], (char * )&ng, (long)sizeof( struct NewGadget ));

		ng.ng_VisualInfo = VisualInfo;
		ng.ng_TextAttr   = &Ma_police;
		ng.ng_LeftEdge  += OffX;
		ng.ng_TopEdge   += OffY;


		PlateauGadgets[ lc ] = g = CreateGadgetA( (ULONG)PlateauGTypes[ lc ],
									g,
									&ng,
									(struct TagItem *)&PlateauGTags[ tc ] );


		while( PlateauGTags[ tc ] )
			tc += 2;
		tc++;

		if( NOT g )
			return( 2L );
	}


	if( ! ( FenetrePlateau = OpenWindowTags( NULL,
								 WA_Left,         Config.FenetrePlateauLeft,
								 WA_Top,          Config.FenetrePlateauTop,
								 WA_Width,        640,
								 WA_Height,       234 + OffY,
								 WA_IDCMP,        SCROLLERIDCMP|
												  IDCMP_GADGETDOWN|
												  IDCMP_MOUSEBUTTONS|
												  IDCMP_GADGETUP|
												  IDCMP_CLOSEWINDOW|
												  IDCMP_REQVERIFY|
												  IDCMP_REFRESHWINDOW,

								 WA_Flags,        WFLG_DRAGBAR|
												  WFLG_DEPTHGADGET|
												  WFLG_CLOSEGADGET|
												  WFLG_SMART_REFRESH|
												  WFLG_ACTIVATE,
								 WA_Gadgets,      PlateauGList,
								 WA_Title,        "Vie III par Tygre",
								 WA_CustomScreen, Mon_ecran,
							     TAG_DONE ) ) )
		return( 4L );

	GT_RefreshWindow( FenetrePlateau, NULL );



	/* --------------------------------------------------------- */
	/* On initialise les scrollers avec les valeurs de la config */
	/* et les gadgets non-encore s�lectionnables                 */
	/* --------------------------------------------------------- */

	GT_SetGadgetAttrs(
				(struct Gadget *)PlateauGadgets[GD_ScrollerH],
				(struct Window *)FenetrePlateau,
				NULL,
				GTSC_Total,   Config.ResolutionH,
				GTSC_Visible, Config.VisibleH,
				TAG_END	);
	GT_SetGadgetAttrs(
				(struct Gadget *)PlateauGadgets[GD_ScrollerV],
				(struct Window *)FenetrePlateau,
				NULL,
				GTSC_Total,   Config.ResolutionV,
				GTSC_Visible, Config.VisibleV,
				TAG_END	);

	if( NBCels == 0)
		Fantomize( TRUE );
	else
		Fantomize( FALSE );



	OffX = FenetrePlateau->BorderLeft;
	OffY = FenetrePlateau->BorderTop;

	DessinsPlateau();

	return( 0L );
}



/* ------------------------------ */
/* Pour afficher les `BevelBoxes` */
/* ------------------------------ */

void DessinsPlateau( void )
{
	DrawBevelBox( FenetrePlateau->RPort, OffX + 468, OffY + 201, 141,  24, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetrePlateau->RPort, OffX + 468, OffY + 172, 141,  24, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetrePlateau->RPort, OffX + 468, OffY + 143, 141,  24, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetrePlateau->RPort, OffX + 468, OffY + 114, 141,  24, GT_VisualInfo, VisualInfo, TAG_DONE );
	DrawBevelBox( FenetrePlateau->RPort, OffX + 468, OffY + 9,   141, 101, GT_VisualInfo, VisualInfo, TAG_DONE );
	/* Le plateau */
	DrawBevelBox( FenetrePlateau->RPort, OffX + 41,  OffY + 8,   405, 203, GT_VisualInfo, VisualInfo, GTBB_Recessed, TRUE, TAG_DONE );
}



/* ---------------------- */
/* Pour fermer la fen�tre */
/* ---------------------- */

void FermePlateau( void )
{
	if( FenetrePlateau )
	{
		Config.FenetrePlateauLeft = FenetrePlateau->LeftEdge;
		 Config.FenetrePlateauTop = FenetrePlateau->TopEdge;
		CloseWindow( FenetrePlateau );
		FenetrePlateau = NULL;
	}

	if( PlateauGList )
	{
		FreeGadgets( PlateauGList );
		PlateauGList = NULL;
	}
}



/* ================================================================================ */
/*                 Pour mettre en ghost ou en normal les boutons				*/
/* ================================================================================ */



void Fantomize( Shadow )

BOOL Shadow;

{
	BYTE NB;

	for( NB = GD_Nouveau; NB <= GD_Quitter; NB++ )
		OnGadget( PlateauGadgets[NB], FenetrePlateau, NULL );

	if( Shadow )
	{
		OffGadget( PlateauGadgets[GD_Nouveau], FenetrePlateau, NULL );
		for( NB = GD_Sauver; NB <= GD_Vivre; NB++ )
			OffGadget( PlateauGadgets[NB], FenetrePlateau, NULL );
	}
}
